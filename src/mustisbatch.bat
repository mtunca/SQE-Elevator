@echo off
for /r %%a in (*.java) do (
echo ---- %%a before ----
type "%%a"
echo --------------------

(
echo /**
echo * @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
echo * @date: 22.01.2017
echo */
)> "%%a.tmp"
type "%%a" >> "%%a.tmp"
del "%%a"
move "%%a.tmp" "%%a"

echo ---- %%a affter ----
type "%%a"
echo --------------------
)