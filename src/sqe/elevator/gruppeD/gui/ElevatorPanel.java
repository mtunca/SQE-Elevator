/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sqe.elevator.gruppeD.data.ElevatorData;
import sqelevator.IElevator;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * Class is used to set the properties of the elevator panel in the gui.
 * This consists of changing the coloring of the buttons, the display for the current weight in the cabin,...
 */
public class ElevatorPanel extends JPanel {
	private static final long serialVersionUID = 8462604213703962342L;

	private JPanel mSymbols;
	
	private JLabel mSpeed;
	private JLabel mWeight;
	
	private ImageIcon mIconDoor;
	private String 	  mIconNameDoor;
	private ImageIcon mIconDirection;
	private String 	  mIconNameDirection;
	
	private JLabel mDoor;
	private JLabel mDirection;
	
	public ElevatorPanel () {
		super(new GridLayout(3,1));
		
		InitComponents();
		AddStyle();
		
		AddComponents();
	}
	
	/**
	 * change the look of the controls regarding the current states of the elevator
	 * @param elevator
	 */
	public void SetData (ElevatorData elevator) {
		mSpeed.setText(elevator.mSpeed + "");
		mWeight.setText(elevator.mWeight + "");
		
		if (elevator.mDoorStatus == IElevator.ELEVATOR_DOORS_OPEN) {
			mIconNameDoor = "doors_opened.png";
		}
		else {
			mIconNameDoor = "doors_closed.png";
		}
		mIconDoor = new ImageIcon("images/" + mIconNameDoor);
		mDoor.setIcon(mIconDoor);
		
		if (elevator.mCommittedDirection == IElevator.ELEVATOR_DIRECTION_UP) {
			mIconNameDirection = "dir_up.png";
		}
		else if (elevator.mCommittedDirection == IElevator.ELEVATOR_DIRECTION_DOWN) {
			mIconNameDirection = "dir_down.png";
		}
		else {
			mIconNameDirection = "dir_none.png";
		}
		mIconDirection = new ImageIcon("images/" + mIconNameDirection);
		mDirection.setIcon(mIconDirection);
	}
	
	private void AddComponents () {
		JPanel empty = new JPanel();
		empty.setPreferredSize(new Dimension(5, 60));
		empty.setBackground(new Color(255,255,255));
		mSymbols.add(mDoor);
		mSymbols.add(empty);
		mSymbols.add(mDirection);
		
		this.add(mSpeed);
		this.add(mWeight);
		this.add(mSymbols);
	}
	
	/**
	 * adding a unified style to the elevator panel
	 */
	private void AddStyle () {
		Color white = new Color(255,255,255);
		Dimension dim = new Dimension(95, 36);
		this.setBackground(white);
		this.setSize(95, 120);
		
		mSpeed.setFont(new Font("Arial", Font.PLAIN, 40));
		mSpeed.setBackground(white);
		mSpeed.setPreferredSize(dim);
		
		mWeight.setFont(new Font("Arial", Font.PLAIN, 40));
		mWeight.setBackground(white);
		mWeight.setPreferredSize(dim);

		mSymbols.setBackground(white);
		mSymbols.setSize(new Dimension(95,60));

		dim = new Dimension(45, 60);
		mDoor.setBackground(white);
		mDoor.setPreferredSize(dim);

		mDirection.setBackground(white);
		mDirection.setPreferredSize(dim);
	}
	
	/**
	 * init configuration respectively style of the text-fields when having no test data
	 */
	private void InitComponents () {		
		mSpeed = new JLabel("", JLabel.CENTER);
		mWeight = new JLabel("", JLabel.CENTER);
		
		mSymbols = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		
		mDoor = new JLabel("", JLabel.CENTER);
		mDirection = new JLabel("", JLabel.CENTER);
		
		mIconNameDoor = "";
		mIconNameDirection = "";
	}

	/** access functions for testing **/
	public String testAccessSpeed () {
		return mSpeed.getText();
	}
	
	public String testAccessWeight () {
		return mWeight.getText();
	}
	
	public String testAccessDoorIcon () {
		return mIconNameDoor;
	}
	
	public String testAccessDirectionIcon () {
		return mIconNameDirection;
	}
}
