/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sqe.elevator.gruppeD.data.ButtonState;


/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * implements the floor panel GUI 
 */
public class FloorPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3665825031655732644L;
	private ISetTarget mParent;

	private int 		mNumFloor;
	private int 		mNumElevator;
	private String 		mFloorName;
	
	private ImageIcon 	mIconFloor;
	private String 		mIconNameFloor;
	private ImageIcon[] mIconStop;
	private String[]	mIconNameStop;
	private ImageIcon[] mIconPosition;
	private String[]	mIconNamePosition;
	
	private JLabel 		mFloorBtns;
	private JLabel 		mFloorNum;
	
	private JButton[] 	mElevatorStopBtn;
	private JButton[] 	mElevatorPosition;
	
	public FloorPanel (ISetTarget parent, int floorNum, boolean disabledDown, boolean disabledUp, int numElevator) {
		super((LayoutManager) new FlowLayout(FlowLayout.CENTER, 0, 0));
		mParent = parent;
		mNumElevator = numElevator;
		mNumFloor = floorNum;
		
		InitComponents(floorNum, disabledDown, disabledUp);
		AddStyle();
		
		InitData();
		
		AddComponents();
	}
	
	/**
	 * setting the look of the call requests for a floor
	 * @param down
	 * @param up
	 */
	public void SetBtns (ButtonState down, ButtonState up) {
		mIconNameFloor = "floor";
		
		if (down == ButtonState.ON) {
			mIconNameFloor += "_down";
		}
		
		if (up == ButtonState.ON) {
			mIconNameFloor += "_up";
		}
		
		if ((down == ButtonState.OFF) && (up == ButtonState.OFF)) {
			mIconNameFloor += "_none";
		}
		
		mIconNameFloor += mFloorName;
		
		mIconFloor = new ImageIcon("images/" + mIconNameFloor);
		
		mFloorBtns.setIcon(mIconFloor);
	}
	
	/**
	 * for a given elevator position, the target floor or the current floor is marked in the gui
	 * @param id position of the elevator
	 * @param isTarget
	 * @param isCurrent
	 */
	public void SetElevatorPosition (int id, boolean isTarget, boolean isCurrent) {
		if ((id < 0) || (mNumElevator <= id)) {
			return;
		}
		
		mIconNamePosition[id] = "position";
		
		if (isCurrent) {
			mIconNamePosition[id] += "_current.png";
		}
		else if (isTarget) {
			mIconNamePosition[id] += "_target.png";
		}
		else {
			mIconNamePosition[id] += "_none.png";
		}
		
		mIconPosition[id] = new ImageIcon("images/" + mIconNamePosition[id]);
		
		mElevatorPosition[id].setIcon(mIconPosition[id]);
	}
	
	/**
	 * set the state of the elevator stop button for a given index of the button
	 * either if it is the target or the pressed floor
	 * @param id
	 * @param isTarget
	 * @param isPressed
	 */
	public void SetElevatorStopBtn (int id, boolean isTarget, boolean isPressed) {
		if ((id < 0) || (mNumElevator <= id)) {
			return;
		}
		
		mIconNameStop[id] = "stop";
		
		if ((isTarget) && (isPressed)) {
			mIconNameStop[id] += "_target_pressed.png";
		}
		else if (isTarget) {
			mIconNameStop[id] += "_target.png";
		}
		else if (isPressed) {
			mIconNameStop[id] += "_pressed.png";
		}
		else {
			mIconNameStop[id] += "_none.png";
		}
		
		mIconStop[id] = new ImageIcon("images/" + mIconNameStop[id]);
		
		mElevatorStopBtn[id].setIcon(mIconStop[id]);
	}
	
	/**
	 * adding and configuring the components in the floor panel generically
	 */
	private void AddComponents () {
		Dimension dim = new Dimension(10, 55);
		JLabel lbl = new JLabel("");
		lbl.setPreferredSize(dim);
		this.add(lbl);
		
		this.add(mFloorBtns);
		
		lbl = new JLabel("");
		lbl.setPreferredSize(dim);
		this.add(lbl);
		
		this.add(mFloorNum);
		
		lbl = new JLabel("");
		lbl.setPreferredSize(dim);
		this.add(lbl);
		
		for (int i=0; i<mNumElevator; ++i) {
			mElevatorStopBtn[i].addActionListener(this);
			this.add(mElevatorStopBtn[i]);
			
			lbl = new JLabel("");
			lbl.setPreferredSize(new Dimension(5,55));
			this.add(lbl);
			
			mElevatorPosition[i].addActionListener(this);
			this.add(mElevatorPosition[i]);

			lbl = new JLabel("");
			lbl.setPreferredSize(dim);
			this.add(lbl);
		}
	}
	
	/**
	 * method to add a unified style for all floor panels and their contents
	 */
	private void AddStyle () {
		int height = 61;
		Dimension dim = new Dimension(45, height);
		Color white = new Color(255,255,255);
		
		this.setBackground(white);
		this.setSize(120 + 100*mNumElevator, height);
		
		mFloorBtns.setPreferredSize(new Dimension(45, height));

		mFloorNum.setFont(new Font("Arial", Font.PLAIN, 40));
		mFloorNum.setPreferredSize(dim);
		
		for (int i=0; i<mNumElevator; ++i) {
			mElevatorStopBtn[i].setBorder(null);
			mElevatorStopBtn[i].setBackground(white);
			mElevatorStopBtn[i].setPreferredSize(dim);
			mElevatorStopBtn[i].setFocusPainted(false);
			mElevatorStopBtn[i].setContentAreaFilled(false);
			
			mElevatorPosition[i].setBorder(null);
			mElevatorPosition[i].setBackground(white);
			mElevatorPosition[i].setPreferredSize(dim);
			mElevatorPosition[i].setFocusPainted(false);
			mElevatorPosition[i].setContentAreaFilled(false);
		}
	}
	
	/**
	 * initialization and creation of the floor panel controls
	 * @param floorNum for a given floor number, on desire up and down buttons can be disabled
	 * @param disabledDown
	 * @param disabledUp
	 */
	private void InitComponents (int floorNum, boolean disabledDown, boolean disabledUp) {
		mIconStop = new ImageIcon[mNumElevator];
		mIconPosition = new ImageIcon[mNumElevator];
		
		mElevatorStopBtn = new JButton[mNumElevator];
		mElevatorPosition = new JButton[mNumElevator];
		mIconNameStop = new String[mNumElevator];
		mIconNamePosition = new String[mNumElevator];
		
		for (int i=0; i<mNumElevator; ++i) {
			mElevatorStopBtn[i] = new JButton("");
			mIconNameStop[i] = "";
			mElevatorPosition[i] = new JButton("");
			mIconNamePosition[i] = "";
		}
		
		// floor png appendix
		mFloorName = ".png";
		if (disabledUp) {
			mFloorName = "_up-disabled" + mFloorName;
		}
		if (disabledDown) {
			mFloorName = "_down-disabled" + mFloorName;
		}
		
		mFloorNum = new JLabel(floorNum + "", JLabel.RIGHT);
		
		mFloorBtns = new JLabel("");
		mIconNameFloor = "";
	}
	
	/**
	 * for all buttons and elevators set an initial state by turning every single button to the state OFF
	 * and the targets and pressed stop buttons to false
	 */
	private void InitData () {
		SetBtns(ButtonState.OFF, ButtonState.OFF);

		// instantiate the panel
		for (int i=0; i<mNumElevator; ++i) {
			SetElevatorPosition(i, false, false);
			SetElevatorStopBtn(i, false, false);
		}		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for (int i=0; i<mNumElevator; ++i) {
			if (e.getSource() == mElevatorStopBtn[i]) {
				mParent.SetTarget(mNumFloor, i);
			}
			else if (e.getSource() == mElevatorPosition[i]) {
				mParent.SetTarget(mNumFloor, i);
			}
		}
	}

	/** access functions for testing **/
	public String testAccessPositionIcon (int id) {		
		return mIconNamePosition[id];
	}
	
	public String testAccessStopIcon (int id) {		
		return mIconNameStop[id];
	}
	
	public String testAccessFloorIcon () {
		return mIconNameFloor;
	}
	
	public String testAccessFloor () {
		return mFloorNum.getText();
	}
	
	public int testAccessNumElevator () {
		return mNumElevator;
	}
	
	public JButton testAccessStopBtn (int id) {
		return mElevatorStopBtn[id];
	}
	
	public JButton testAccessPosition (int id) {
		return mElevatorPosition[id];
	}
}
