/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.gui.FloorPanel;

/**
 * Test class for testing the floor panel when neither the down or up button on the floor are disabled
 * class to test the floor panel, regarding the pressed buttons and the floor targets
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 */
public class FloorPanelMiddleTest {
	private FloorPanel testPanel;
	private String floorAppendix;
	private int elevatorId;
	private BuildingData building;
	
	@Before
	public void setUp() throws Exception {
		building = new BuildingData(3, 1, 1);
		
		testPanel = new FloorPanel(new SetTargetDummy(), 1, false, false, building.mNumElevators);
		floorAppendix = ".png"; // used for disabled floors
		elevatorId = 0;
	}
	
	@Test
	public void testNotInitialized () {
		assertEquals(building.mNumFloors + "", testPanel.testAccessFloor());
		assertEquals("floor_none" + floorAppendix, testPanel.testAccessFloorIcon());
		assertEquals(building.mNumElevators, testPanel.testAccessNumElevator());
		
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
	}
	
	@Test
	public void testFloorBtnsOnOn () {
		testPanel.SetBtns(ButtonState.ON, ButtonState.ON);
		
		assertEquals("floor_down_up" + floorAppendix, testPanel.testAccessFloorIcon());
	}

	@Test
	public void testFloorBtnsOnOff () {
		testPanel.SetBtns(ButtonState.ON, ButtonState.OFF);
		
		assertEquals("floor_down" + floorAppendix, testPanel.testAccessFloorIcon());
	}

	@Test
	public void testFloorBtnsOffOn () {
		testPanel.SetBtns(ButtonState.OFF, ButtonState.ON);
		
		assertEquals("floor_up" + floorAppendix, testPanel.testAccessFloorIcon());
	}

	@Test
	public void testFloorBtnsOffOff () {
		testPanel.SetBtns(ButtonState.OFF, ButtonState.OFF);
		
		assertEquals("floor_none" + floorAppendix, testPanel.testAccessFloorIcon());
	}

	@Test
	public void testPositionNoTargetNotCurrent () {
		testPanel.SetElevatorPosition(elevatorId, false, false);
		
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testPositionTargetNotCurrent () {
		testPanel.SetElevatorPosition(elevatorId, true, false);
		
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testPositionNoTargetCurrent () {
		testPanel.SetElevatorPosition(elevatorId, false, true);
		
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testPositionTargetCurrent () {
		testPanel.SetElevatorPosition(elevatorId, true, true);
		
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testStopNoTargetNotPressed() {
		testPanel.SetElevatorStopBtn(elevatorId, false, false);
		
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
	}

	@Test
	public void testStopTargetNotPressed() {
		testPanel.SetElevatorStopBtn(elevatorId, true, false);
		
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(elevatorId));
	}

	@Test
	public void testStopNoTargetPressed() {
		testPanel.SetElevatorStopBtn(elevatorId, false, true);
		
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(elevatorId));
	}

	@Test
	public void testStopTargetPressed() {
		testPanel.SetElevatorStopBtn(elevatorId, true, true);
		
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(elevatorId));
	}
}
