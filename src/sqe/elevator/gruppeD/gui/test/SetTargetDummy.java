/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import sqe.elevator.gruppeD.gui.ISetTarget;

public class SetTargetDummy implements ISetTarget {
	int mFloor;
	int mElevator;

	@Override
	public void SetTarget(int floor, int elevator) {
		mFloor = floor;
		mElevator = elevator;
	}

}
