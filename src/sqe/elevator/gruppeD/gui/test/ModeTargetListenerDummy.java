/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import sqe.elevator.gruppeD.data.Mode;
import sqe.elevator.gruppeD.gui.IModeTargetListener;

public class ModeTargetListenerDummy implements IModeTargetListener{
	Mode mMode;
	int mFloor;
	int mElevator;

	@Override
	public void ModeListener(Mode mode) {
		mMode = mode;
	}

	@Override
	public void TargetListener(int floor, int elevator) {
		mFloor = floor;
		mElevator = elevator;		
	}

}
