/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.ElevatorData;
import sqe.elevator.gruppeD.gui.ElevatorPanel;
import sqelevator.IElevator;

/**
 * Testing class to test the ElevatorPanel GUI logic.
 * This captures both the text in the text controls, and the current used icon in a button for a given state
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 */
public class ElevatorPanelTest {
	private ElevatorData testData;
	private ElevatorPanel testPanel;
	
	@Before
	public void setUp() throws Exception {
		testPanel = new ElevatorPanel();
		testData = new ElevatorData();
	}
	
	@Test
	public void testNotInitialized () {
		assertEquals("", testPanel.testAccessSpeed());
		assertEquals("", testPanel.testAccessWeight());
		assertEquals("", testPanel.testAccessDoorIcon());
		assertEquals("", testPanel.testAccessDirectionIcon());
	}
	
	@Test
	public void testSpeedWeight () {
		testData.mSpeed = 20;
		testData.mWeight = 100;
		
		testPanel.SetData(testData);
		
		assertEquals("20", testPanel.testAccessSpeed());
		assertEquals("100", testPanel.testAccessWeight());
	}

	@Test
	public void testDoorOpen() {
		testData.mDoorStatus = IElevator.ELEVATOR_DOORS_OPEN;
		
		testPanel.SetData(testData);
		
		assertEquals("doors_opened.png", testPanel.testAccessDoorIcon());
	}

	@Test
	public void testDoorClosed() {
		testData.mDoorStatus = IElevator.ELEVATOR_DOORS_CLOSED;
		
		testPanel.SetData(testData);
		
		assertEquals("doors_closed.png", testPanel.testAccessDoorIcon());
	}
	
	@Test
	public void testDirectionUp () {
		testData.mCommittedDirection = IElevator.ELEVATOR_DIRECTION_UP;
		
		testPanel.SetData(testData);
		
		assertEquals("dir_up.png", testPanel.testAccessDirectionIcon());
	}
	
	@Test
	public void testDirectionDown () {
		testData.mCommittedDirection = IElevator.ELEVATOR_DIRECTION_DOWN;
		
		testPanel.SetData(testData);
		
		assertEquals("dir_down.png", testPanel.testAccessDirectionIcon());
	}
	
	@Test
	public void testDirectionNone () {
		testData.mCommittedDirection = IElevator.ELEVATOR_DIRECTION_UNCOMMITTED;
		
		testPanel.SetData(testData);
		
		assertEquals("dir_none.png", testPanel.testAccessDirectionIcon());
	}
}
