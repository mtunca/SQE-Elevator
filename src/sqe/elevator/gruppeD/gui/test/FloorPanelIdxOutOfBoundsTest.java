/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.gui.FloorPanel;

/**
 * Test class for checking the handling of invalid elevatorIds
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 */
public class FloorPanelIdxOutOfBoundsTest {
	private FloorPanel testPanel;
	private int elevatorId;
	private BuildingData building;
	
	@Before
	public void setUp() throws Exception {
		building = new BuildingData(3, 1, 1);
		
		testPanel = new FloorPanel(new SetTargetDummy(), 1, false, false, building.mNumElevators);
		elevatorId = 0;
	}

	@Test
	public void testPositionIdxLower () {
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
		testPanel.SetElevatorPosition(-1, true, true);		
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testPositionIdxUpper () {
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
		testPanel.SetElevatorPosition(1, true, true);
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
	}

	@Test
	public void testStopIdxLower() {
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));		
		testPanel.SetElevatorStopBtn(-1, true, true);		
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
	}

	@Test
	public void testStopIdxUpper() {
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
		testPanel.SetElevatorStopBtn(5, true, true);		
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
	}

}
