/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.ElevatorControlCenter;
import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.data.DataWrapper;
import sqe.elevator.gruppeD.data.ElevatorData;
import sqe.elevator.gruppeD.gui.ElevatorGui;
import sqe.elevator.gruppeD.test.IElevatorEmptyDummy;
import sqelevator.IElevator;

public class ElevatorGuiTest {
	private ElevatorGui gui;
	private DataWrapper data;

	@Before
	public void setUp() throws Exception {
		BuildingData building = new BuildingData(3, 2, 5);		
		data = new DataWrapper(building);
		
		IElevator ielev = new IElevatorEmptyDummy();
		
		gui = new ElevatorGui(data, new ElevatorControlCenter(ielev, false, ""));
	}

	@Test
	public void testUpdate() throws Exception {
		Vector<Integer> stop0 = new Vector<Integer>();
		Vector<Integer> stop1 = new Vector<Integer>();
		Vector<Integer> up = new Vector<Integer>();
		Vector<Integer> down = new Vector<Integer>();
		ElevatorData elev0 = new ElevatorData();
		ElevatorData elev1 = new ElevatorData();
		
		elev0.mFloor = 1;
		elev0.mCommittedDirection = IElevator.ELEVATOR_DIRECTION_UP;
		elev0.mDoorStatus = IElevator.ELEVATOR_DOORS_OPEN;
		
		elev1.mFloor = 3;
		elev1.mCommittedDirection = IElevator.ELEVATOR_DIRECTION_DOWN;
		elev1.mDoorStatus = IElevator.ELEVATOR_DOORS_CLOSED;

		elev0.mTarget = 4;
		
		elev1.mTarget = 1;
		
		data.SetElevatorData(0, elev0);
		data.SetElevatorData(1, elev1);

		stop0.add(0);
		stop0.add(2);
		data.SetElevatorBtnsPressed(0, stop0);
		
		stop1.add(0);
		stop1.add(1);
		data.SetElevatorBtnsPressed(1, stop1);
		
		up.add(0);
		up.add(3);
		data.SetFloorBtnsUpPressed(up);
		
		down.add(2);
		down.add(3);
		data.SetFloorBtnsDownPressed(down);
		
		gui.SetData(data);
		gui.Update();
		
		assertEquals("floor_up_down-disabled.png", gui.testAccessFloorIcon(0));
		assertEquals("floor_none.png", gui.testAccessFloorIcon(1));
		assertEquals("floor_down.png", gui.testAccessFloorIcon(2));
		assertEquals("floor_down_up.png", gui.testAccessFloorIcon(3));
		assertEquals("floor_none_up-disabled.png", gui.testAccessFloorIcon(4));
		
		assertEquals("stop_pressed.png", gui.testAccessFloorElevatorStopIcon(0, 0));
		assertEquals("stop_none.png", gui.testAccessFloorElevatorStopIcon(1, 0));
		assertEquals("stop_pressed.png", gui.testAccessFloorElevatorStopIcon(2, 0));
		assertEquals("stop_none.png", gui.testAccessFloorElevatorStopIcon(3, 0));
		assertEquals("stop_target.png", gui.testAccessFloorElevatorStopIcon(4, 0));
		
		assertEquals("stop_pressed.png", gui.testAccessFloorElevatorStopIcon(0, 1));
		assertEquals("stop_target_pressed.png", gui.testAccessFloorElevatorStopIcon(1, 1));
		assertEquals("stop_none.png", gui.testAccessFloorElevatorStopIcon(2, 1));
		assertEquals("stop_none.png", gui.testAccessFloorElevatorStopIcon(3, 1));
		assertEquals("stop_none.png", gui.testAccessFloorElevatorStopIcon(4, 1));
		
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(0, 0));
		assertEquals("position_current.png", gui.testAccessFloorElevatorPositionIcon(1, 0));
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(2, 0));
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(3, 0));
		assertEquals("position_target.png", gui.testAccessFloorElevatorPositionIcon(4, 0));
		
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(0, 1));
		assertEquals("position_target.png", gui.testAccessFloorElevatorPositionIcon(1, 1));
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(2, 1));
		assertEquals("position_current.png", gui.testAccessFloorElevatorPositionIcon(3, 1));
		assertEquals("position_none.png", gui.testAccessFloorElevatorPositionIcon(4, 1));
		
		assertEquals("dir_up.png", gui.testAccessElevatorDirectionIcon(0));
		assertEquals("dir_down.png", gui.testAccessElevatorDirectionIcon(1));
		
		assertEquals("doors_opened.png", gui.testAccessElevatorDoorIcon(0));
		assertEquals("doors_closed.png", gui.testAccessElevatorDoorIcon(1));
		
		assertEquals(elev0.mSpeed + "", gui.testAccessElevatorSpeed(0));
		assertEquals(elev1.mSpeed + "", gui.testAccessElevatorSpeed(1));
		
		assertEquals(elev0.mWeight + "", gui.testAccessElevatorWeight(0));
		assertEquals(elev1.mWeight + "", gui.testAccessElevatorWeight(1));
		
		assertEquals(2, gui.testAccessFloorNumElevator(0));
		assertEquals(2, gui.testAccessFloorNumElevator(1));
		assertEquals(2, gui.testAccessFloorNumElevator(2));
		assertEquals(2, gui.testAccessFloorNumElevator(3));
		assertEquals(2, gui.testAccessFloorNumElevator(4));
		
		assertEquals("0", gui.testAccessFloorText(0));
		assertEquals("1", gui.testAccessFloorText(1));
		assertEquals("2", gui.testAccessFloorText(2));
		assertEquals("3", gui.testAccessFloorText(3));
		assertEquals("4", gui.testAccessFloorText(4));
	}

}
