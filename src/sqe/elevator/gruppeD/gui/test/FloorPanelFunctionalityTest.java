/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import javax.swing.JButton;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.gui.FloorPanel;

public class FloorPanelFunctionalityTest {
	private FloorPanel testPanel;
	private BuildingData building;
	private SetTargetDummy dummy;
	private int floorNum;
	
	@Before
	public void setUp() throws Exception {
		building = new BuildingData(3, 2, 1);
		dummy = new SetTargetDummy();
		floorNum = 1;
		
		testPanel = new FloorPanel(dummy, floorNum, false, false, building.mNumElevators);
	}

	@Test
	public void testSetTarget() {
		JButton stop0 = testPanel.testAccessStopBtn(0);
		JButton stop1 = testPanel.testAccessPosition(1);
		JButton pos0 = testPanel.testAccessPosition(0);
		JButton pos1 = testPanel.testAccessPosition(1);
		
		stop0.doClick();
		
		assertEquals(floorNum, dummy.mFloor);
		assertEquals(0, dummy.mElevator);
		
		stop1.doClick();
		
		assertEquals(floorNum, dummy.mFloor);
		assertEquals(1, dummy.mElevator);
		
		pos0.doClick();
		
		assertEquals(floorNum, dummy.mFloor);
		assertEquals(0, dummy.mElevator);
		
		pos1.doClick();
		
		assertEquals(floorNum, dummy.mFloor);
		assertEquals(1, dummy.mElevator);
	}

}
