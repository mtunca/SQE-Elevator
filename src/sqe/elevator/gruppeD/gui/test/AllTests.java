/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ElevatorPanelTest.class,
	FloorPanelTopTest.class,
	FloorPanelMiddleTest.class,
	FloorPanelBottomTest.class,
	FloorPanelMultipleElevatorsTest.class,
	FloorPanelIdxOutOfBoundsTest.class,
	FloorPanelFunctionalityTest.class,
	ElevatorGuiTest.class, 
	ElevatorGuiFunctionalityTest.class
})
public class AllTests {

}
