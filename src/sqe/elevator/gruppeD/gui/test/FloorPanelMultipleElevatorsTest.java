/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.gui.FloorPanel;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * test class to test more than one elevator, check that changes to one elevator do not affect the other
 */
public class FloorPanelMultipleElevatorsTest {
	private FloorPanel testPanel;
	private String floorAppendix;
	private BuildingData building;
	
	@Before
	public void setUp() throws Exception {
		building = new BuildingData(3, 2, 1);
		
		testPanel = new FloorPanel(new SetTargetDummy(), 1, false, false, building.mNumElevators);
		floorAppendix = ".png"; // used for disabled floors
	}
	
	@Test
	public void testNotInitialized () {
		assertEquals(building.mNumFloors + "", testPanel.testAccessFloor());
		assertEquals("floor_none" + floorAppendix, testPanel.testAccessFloorIcon());
		assertEquals(building.mNumElevators, testPanel.testAccessNumElevator());
		
		for (int elevatorId=0; elevatorId<building.mNumElevators; ++elevatorId) {
			assertEquals("position_none.png", testPanel.testAccessPositionIcon(elevatorId));
			assertEquals("stop_none.png", testPanel.testAccessStopIcon(elevatorId));
		}
	}

	@Test
	public void testPositionNoTargetNotCurrent () {
		testPanel.SetElevatorPosition(0, false, false);

		testPanel.SetElevatorPosition(1, false, false);
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, false);
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, false, true);
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, true);
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));
	}

	@Test
	public void testPositionTargetNotCurrent () {
		testPanel.SetElevatorPosition(0, true, false);

		testPanel.SetElevatorPosition(1, false, false);
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, false);
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, false, true);
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, true);
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));
	}

	@Test
	public void testPositionNoTargetCurrent () {
		testPanel.SetElevatorPosition(0, false, true);

		testPanel.SetElevatorPosition(1, false, false);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, false);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, false, true);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, true);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));
	}

	@Test
	public void testPositionTargetCurrent () {
		testPanel.SetElevatorPosition(0, true, true);

		testPanel.SetElevatorPosition(1, false, false);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_none.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, false);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_target.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, false, true);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));

		testPanel.SetElevatorPosition(1, true, true);
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(0));
		assertEquals("position_current.png", testPanel.testAccessPositionIcon(1));
	}

	@Test
	public void testStopNoTargetNotPressed() {
		testPanel.SetElevatorStopBtn(0, false, false);

		testPanel.SetElevatorStopBtn(1, false, false);
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, false);
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, false, true);
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, true);
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(1));
	}

	@Test
	public void testStopTargetNotPressed() {
		testPanel.SetElevatorStopBtn(0, true, false);

		testPanel.SetElevatorStopBtn(1, false, false);
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, false);
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, false, true);
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, true);
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(1));
	}

	@Test
	public void testStopNoTargetPressed() {
		testPanel.SetElevatorStopBtn(0, false, true);

		testPanel.SetElevatorStopBtn(1, false, false);
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, false);
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, false, true);
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, true);
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(1));
	}

	@Test
	public void testStopTargetPressed() {
		testPanel.SetElevatorStopBtn(0, true, true);

		testPanel.SetElevatorStopBtn(1, false, false);
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_none.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, false);
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, false, true);
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_pressed.png", testPanel.testAccessStopIcon(1));

		testPanel.SetElevatorStopBtn(1, true, true);
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(0));
		assertEquals("stop_target_pressed.png", testPanel.testAccessStopIcon(1));
	}
}
