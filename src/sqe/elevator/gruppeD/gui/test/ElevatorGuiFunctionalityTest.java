/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui.test;

import static org.junit.Assert.*;

import javax.swing.JButton;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.data.DataWrapper;
import sqe.elevator.gruppeD.data.Mode;
import sqe.elevator.gruppeD.gui.ElevatorGui;

public class ElevatorGuiFunctionalityTest {
	private ElevatorGui testGui;
	private BuildingData building;
	private ModeTargetListenerDummy dummy;
	
	@Before
	public void setUp() throws Exception {
		building = new BuildingData(1, 2, 1);
		DataWrapper data = new DataWrapper(building);
		dummy = new ModeTargetListenerDummy();
		
		testGui = new ElevatorGui(data, dummy);
	}

	@Test
	public void testModeListener() {
		JButton mode = testGui.testAccessModeButton();
		
		mode.doClick();
		assertEquals(dummy.mMode, Mode.AUTOMATIC);
		
		mode.doClick();
		assertEquals(dummy.mMode, Mode.MANUAL);

		mode.doClick();
		assertEquals(dummy.mMode, Mode.AUTOMATIC);
		
		mode.doClick();
		assertEquals(dummy.mMode, Mode.MANUAL);
	}
	
	@Test
	public void testTargetListener() {
		testGui.SetTarget(1, 2);
		
		assertEquals(1, dummy.mFloor);
		assertEquals(2, dummy.mElevator);

		testGui.SetTarget(4, 5);
		
		assertEquals(4, dummy.mFloor);
		assertEquals(5, dummy.mElevator);

		testGui.SetTarget(-1, 3);
		
		assertEquals(-1, dummy.mFloor);
		assertEquals(3, dummy.mElevator);
	}

}
