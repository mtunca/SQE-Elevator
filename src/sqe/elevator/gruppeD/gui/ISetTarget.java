/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui;

public interface ISetTarget {

	/**
	 * change the target of an elevator, notifies the ElevatorControl about the new target
	 * @param floor id of the target floor
	 * @param elevator id of the elevator, which shoudl receive the change
	 */
	void SetTarget(int floor, int elevator);

}