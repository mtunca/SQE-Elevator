/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui;

import sqe.elevator.gruppeD.data.Mode;

public interface IModeTargetListener {

	/**
	 * listen to the mode setting of the gui
	 * @param mode
	 * @throws Exception 
	 */
	void ModeListener(Mode mode);

	/**
	 * for a given elevator id, the target-floor is set, by the set floor variable
	 * @param floor target position to be set
	 * @param elevator id of the elevator, which gets an update of the target-floor
	 * @throws Exception 
	 */
	void TargetListener(int floor, int elevator);

}