/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.data.DataWrapper;
import sqe.elevator.gruppeD.data.Mode;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * This class implements the GUI functionality of the elevator system in a generical way.
 */
public class ElevatorGui extends JFrame implements ActionListener, ISetTarget {
	private static final long serialVersionUID = -4454002012899421992L;
	
	private IModeTargetListener mECC;
	private DataWrapper 		mData;
	
	private JPanel 				mContentPanel;
	
	private JPanel				mTitlePanel;
	private JLabel				mTitle;
	private Mode 				mCurrentMode;
	private ImageIcon			mModeAutomatic;
	private ImageIcon			mModeManual;
	
	private JScrollPane			mScroll;
	private JPanel				mScrollPanel;
	private JPanel[]			mElevatorPanel;
	private JPanel[]			mFloorPanel;
	
	private JButton   			mMode;
	private ElevatorPanel[][]	mElevator;
	private FloorPanel[]    	mFloor;
	
	private int 				mNumPanels;
	private int					mMaxFloors = 11;
	
	/**
	 * constructor for creating the elevators in a generical way by the given properties in the mData.mData.mBuilding variable
	 * @param mData.mData.mBuilding properties of the elevator system to be generated
	 * @param control 
	 */
	public ElevatorGui (DataWrapper data, IModeTargetListener ecc) {
		super();
		mECC = ecc;
		mData = data;

		mNumPanels = (int) Math.ceil((((double) mData.mBuilding.mNumFloors) / mMaxFloors));

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		InitComponents();
		AddStyle();
		
		AddComponents();
	}
	
	public void SetData (DataWrapper data) {
		mData = data;
		
		for (int id=0; id<mData.mBuilding.mNumElevators; ++id) {
			for (int i=0; i<mNumPanels; ++i) {
				mElevator[id][i].SetData(mData.mElevatorList[id]);
			}
		}
	}

	/**
	 * adding components to the gui such as the floorpanel or the elevator panel
	 */
	private void AddComponents () {
		GridBagConstraints cContent = new GridBagConstraints();
		GridBagConstraints cScroll = new GridBagConstraints();
		Color white = new Color(255,255,255);
		Dimension dim = new Dimension(120, 120);
		
		mMode.addActionListener(this);
		
		JPanel empty;
		
		for (int i=0; i<mNumPanels; ++i) {
			empty = new JPanel();
			empty.setPreferredSize(dim);
			empty.setBackground(white);
			mElevatorPanel[i].add(empty);
		}
		
		dim = new Dimension(10, 120);
		for (int i=0; i<mData.mBuilding.mNumElevators; ++i) {
			// don't add multiple instances of the elevatorpanel
			//for (int j=0; j<mNumPanels; ++j) {
				mElevatorPanel[0].add(mElevator[i][0]);
				empty = new JPanel();
				empty.setPreferredSize(dim);
				empty.setBackground(white);
				mElevatorPanel[0].add(empty);
			//}
		}

		int panel = 0;
		int offset = mData.mBuilding.mNumFloors-(mNumPanels-1)*mMaxFloors;
		for (int i=mData.mBuilding.mNumFloors-1; i>=0; --i) {
			mFloorPanel[panel].add(mFloor[i]);
			
			if (((i-offset) % mMaxFloors) == 0) {
				panel++;
			}
		}

		mTitlePanel.add(mTitle);
		mTitlePanel.add(mMode);

		cContent.fill = GridBagConstraints.NONE;
		cContent.anchor = GridBagConstraints.FIRST_LINE_START;
		
		cContent.gridx = 0; cContent.gridy = 0;
		cContent.gridwidth = 1; cContent.gridheight = 1;
		mContentPanel.add(mTitlePanel, cContent);
		
		cScroll.fill = GridBagConstraints.NONE;
		cScroll.anchor = GridBagConstraints.FIRST_LINE_START;
		cScroll.gridwidth = 1;
		for (int i=0; i<mNumPanels; ++i) {
			cScroll.gridx = i;
			
			if (i == mNumPanels-1) {
				cScroll.weightx = 1.0;
			}
			
			cScroll.gridy = 0;
			cScroll.gridheight = 2;
			cScroll.weighty = 0;
			mScrollPanel.add(mElevatorPanel[i], cScroll);
			
			cScroll.gridy = 2;
			cScroll.gridheight = mMaxFloors;
			cScroll.weighty = 1.0;
			mScrollPanel.add(mFloorPanel[i], cScroll);
		}
		
		cContent.gridx = 0; cContent.gridy = 1;
		cContent.gridwidth = 1; cContent.gridheight = 2+mMaxFloors;
		cContent.weighty = 1.0; cContent.weightx = 1.0;
		cContent.fill = GridBagConstraints.BOTH;
		mContentPanel.add(mScroll, cContent);
		
		this.add(mContentPanel);
	}	

	/**
	 * adding a unified style for all controls
	 */
	private void AddStyle () {
		this.setSize(new Dimension(1760, 946));
		this.setResizable(false);
		Color white = new Color(255,255,255);
		this.setBackground(white);
		mContentPanel.setBackground(white);
		
		mTitlePanel.setSize(new Dimension(1760, 61));
		mTitlePanel.setBackground(white);
		
		mTitle.setFont(new Font("Arial", Font.PLAIN, 40));
		mTitle.setBackground(white);
		
		mMode.setBorder(null);
		mMode.setBackground(white);
		mMode.setSize(359, 61);
		mMode.setFocusPainted(false);
		mMode.setContentAreaFilled(false);
		
		
		mScroll.setBorder(null);
		mScroll.setBackground(white);
		mScroll.setSize(new Dimension(1760, 915));
		
		mScrollPanel.setBackground(white);
		
		for (int i=0; i<mNumPanels; ++i) {
			mElevatorPanel[i].setBackground(white);
			mElevatorPanel[i].setSize(120 + 100*mData.mBuilding.mNumElevators, 120);
		}
	}
	
	/**
	 * change the mode on the gui, and notify the ElevatorControl about this change
	 * @param mode
	 */
	private void ChangeMode (Mode mode) {
		mCurrentMode = mode;
		if (mode == Mode.MANUAL) {
			mMode.setIcon(mModeManual);
	
		}
		else {
			mMode.setIcon(mModeAutomatic);
		}

		mECC.ModeListener(mode);
	}

	/* (non-Javadoc)
	 * @see sqe.elevator.gruppeD.gui.ISetTarget#SetTarget(int, int)
	 */
	@Override
	public void SetTarget (int floor, int elevator) {
		mECC.TargetListener(floor, elevator);
	}

	/**
	 * initialization and creation of all GUI controls
	 */
	private void InitComponents () {
		mContentPanel = new JPanel(new GridBagLayout());
		mTitlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		mTitle = new JLabel(" Elevator Control Center");
		
		mScrollPanel = new JPanel(new GridBagLayout());
		mScroll = new JScrollPane(mScrollPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
				
		mMode = new JButton("");
		mModeAutomatic = new ImageIcon("images/mode_automatic.png");
		mModeManual = new ImageIcon("images/mode_manual.png");
		ChangeMode(Mode.MANUAL);
		
		mElevator = new ElevatorPanel[mData.mBuilding.mNumElevators][mNumPanels];
		for (int i=0; i<mData.mBuilding.mNumElevators; ++i) {
			for (int j=0; j<mNumPanels; ++j) {
				mElevator[i][j] = new ElevatorPanel();
			}
		}
		
		mFloor = new FloorPanel[mData.mBuilding.mNumFloors];
		mFloor[0] = new FloorPanel(this, 0, true, false, mData.mBuilding.mNumElevators);
		for (int i=1; i<mData.mBuilding.mNumFloors-1; ++i) {
			mFloor[i] = new FloorPanel(this, i, false, false, mData.mBuilding.mNumElevators);
		}
		mFloor[mData.mBuilding.mNumFloors-1] = new FloorPanel(this, mData.mBuilding.mNumFloors-1, false, true, mData.mBuilding.mNumElevators);
		
		mElevatorPanel = new JPanel[mNumPanels];
		mFloorPanel = new JPanel[mNumPanels];
		for (int i=0; i<mNumPanels; ++i) {
			mElevatorPanel[i] = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			mFloorPanel[i] = new JPanel(new GridLayout(mData.mBuilding.mNumFloors, 1));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mMode) { 
			ChangeMode((mCurrentMode == Mode.AUTOMATIC) ? Mode.MANUAL : Mode.AUTOMATIC);
		}
	}
	

	/**
	 * function used to update the elevator panels in the first glance and the floor panels in the second
	 */
	public void Update () {
		// update elevator panels
		for (int id=0; id<mData.mBuilding.mNumElevators; ++id) {
			for (int i=0; i<mNumPanels; ++i) {
				mElevator[id][i].SetData(mData.mElevatorList[id]);
			}
		}
		
		// update floor panels
		boolean isTarget = false;
		boolean isCurrent = false;
		boolean isPressed = false;
		for(int id=0; id<mData.mBuilding.mNumElevators; ++id) {
			for(int i=0; i<mData.mBuilding.mNumFloors; ++i) {
				mFloor[i].SetBtns(mData.mFloorBtnsDownState[i], mData.mFloorBtnsUpState[i]);

				isTarget = (mData.mElevatorList[id].mTarget == i);
				isCurrent = (mData.mElevatorList[id].mFloor == i);
				isPressed = (mData.mElevatorStopState[id][i] == ButtonState.ON);

				mFloor[i].SetElevatorPosition(id, isTarget, isCurrent);
				mFloor[i].SetElevatorStopBtn(id, isTarget, isPressed);
			}
			
			for(int i=0; i<mElevator[id].length; ++i) {
				mElevator[id][i].SetData(mData.mElevatorList[id]);
			}
		}
	}
	
	public void DisplayError (String msg) {
		JOptionPane pane = new JOptionPane(msg,JOptionPane.ERROR_MESSAGE);
		JDialog dialog = pane.createDialog(null, "ECC Error Handling");
		dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		dialog.setVisible(true);  
	}
	
	/** access functions for testing **/
	public String testAccessFloorElevatorPositionIcon (int floor, int elevator) {
		return mFloor[floor].testAccessPositionIcon(elevator);
	}
	
	public String testAccessFloorElevatorStopIcon (int floor, int elevator) {
		return mFloor[floor].testAccessStopIcon(elevator);
	}
	
	public String testAccessFloorIcon (int floor) {
		return mFloor[floor].testAccessFloorIcon();
	}
	
	public String testAccessFloorText (int floor) {
		return mFloor[floor].testAccessFloor();
	}
	
	public int testAccessFloorNumElevator (int floor) {
		return mFloor[floor].testAccessNumElevator();
	}
	public String testAccessElevatorSpeed (int elevator) {
		return mElevator[elevator][0].testAccessSpeed();
	}
	
	public String testAccessElevatorWeight (int elevator) {
		return mElevator[elevator][0].testAccessWeight();
	}
	
	public String testAccessElevatorDoorIcon (int elevator) {
		return mElevator[elevator][0].testAccessDoorIcon();
	}
	
	public String testAccessElevatorDirectionIcon (int elevator) {
		return mElevator[elevator][0].testAccessDirectionIcon();
	}
	
	public JButton testAccessModeButton () {
		return mMode;
	}
}
