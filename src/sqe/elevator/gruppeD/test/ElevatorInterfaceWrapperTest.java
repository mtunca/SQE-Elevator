/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.ElevatorInterfaceWrapper;

public class ElevatorInterfaceWrapperTest {
	private ElevatorInterfaceWrapper wrapper;
	private IElevatorConnectionDummy conn;
	private IElevatorThrowsRemoteExceptionDummy ieException;
	private IElevatorEmptyDummy ieCheck;
	private IElevatorUnsyncDummy ieUnsync;
	
	@Before
	public void setUp() throws Exception {
		conn = new IElevatorConnectionDummy();
		ieException = new IElevatorThrowsRemoteExceptionDummy();
		ieCheck = new IElevatorEmptyDummy();
		ieUnsync = new IElevatorUnsyncDummy();
	}

	@Test
	public void testCTor() {
		assertEquals(0, conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.ElevatorNum;
		
		wrapper = new ElevatorInterfaceWrapper(ieException, conn);
		assertEquals(1, conn.mCntConnectionLost);
		
		wrapper = new ElevatorInterfaceWrapper(ieCheck, conn);
		assertEquals(1, conn.mCntConnectionLost);
		
	}
	
	@Test
	public void testSetElevatorTarget() {
		assertEquals(0, conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.None;
		
		wrapper = new ElevatorInterfaceWrapper(ieException, conn);
		assertEquals(0, conn.mCntConnectionLost);
		
		wrapper.SetElevatorTarget(-1, 0);
		wrapper.SetElevatorTarget(10, 0);
		wrapper.SetElevatorTarget(0, -1);
		wrapper.SetElevatorTarget(0, 10);
		
		ieException.mExceptionAt = ThrowExceptionAt.setTarget;
		wrapper.SetElevatorTarget(0, 0);
		assertEquals(1, conn.mCntConnectionLost);
	}
	
	@Test
	public void testSetCommittedDirection() {
		assertEquals(0, conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.None;
		
		wrapper = new ElevatorInterfaceWrapper(ieException, conn);
		assertEquals(0, conn.mCntConnectionLost);
		
		wrapper.SetElevatorDirection(0, 1);
		
		ieException.mExceptionAt = ThrowExceptionAt.setCommittedDirection;
		wrapper.SetElevatorDirection(0, 1);
		assertEquals(1, conn.mCntConnectionLost);
	}
	
	@Test
	public void testUpdateExceptions() {
		assertEquals(0, conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.None;
		
		wrapper = new ElevatorInterfaceWrapper(ieException, conn);
		assertEquals(0, conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.ClockTick;
		
		wrapper.GetElevators();
		assertEquals(1, conn.mCntConnectionLost);
		wrapper.SetInterface(ieException, true);
		
		ieException.mExceptionAt = ThrowExceptionAt.None;
		
		wrapper.GetElevatorBtnsPressed(0);
		assertEquals(1, conn.mCntConnectionLost);
		
		wrapper.GetFloorBtnsDownPressed();
		assertEquals(1,conn.mCntConnectionLost);
		
		ieException.mExceptionAt = ThrowExceptionAt.FloorButtonDown;
		ieException.mClockTick++;
		wrapper.GetFloorBtnsUpPressed();
		assertEquals(2,conn.mCntConnectionLost);
		
		wrapper.GetElevators();	
	}
	
	@Test
	public void testUpdateUnsync() {
		assertEquals(0, conn.mCntConnectionSyncLost);
		
		wrapper = new ElevatorInterfaceWrapper(ieUnsync, conn);
		assertEquals(0, conn.mCntConnectionSyncLost);
		
		wrapper.GetElevators();
		assertEquals(1, conn.mCntConnectionSyncLost);
	}
}
