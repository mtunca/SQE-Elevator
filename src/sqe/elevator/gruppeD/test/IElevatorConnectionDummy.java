/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import sqe.elevator.gruppeD.IElevatorConnection;

public class IElevatorConnectionDummy implements IElevatorConnection {
	public int mCntConnectionLost = 0;
	public int mCntConnectionSyncLost = 0;
	
	@Override
	public void ConnectionLost() {
		mCntConnectionLost++;
	}

	@Override
	public void ConnectionSyncLost() {
		mCntConnectionSyncLost++;
	}

}
