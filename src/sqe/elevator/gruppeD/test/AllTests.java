/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	sqe.elevator.gruppeD.gui.test.AllTests.class,
	sqe.elevator.gruppeD.data.test.AllTests.class,
	ElevatorControlCenterTest.class,
	ElevatorControlCenterReconnectTest.class,
	DummyInterfaceTest.class,
	ElevatorInterfaceWrapperTest.class
})
public class AllTests {

}
