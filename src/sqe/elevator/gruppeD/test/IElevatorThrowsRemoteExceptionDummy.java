/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import java.rmi.RemoteException;

import sqelevator.IElevator;

public class IElevatorThrowsRemoteExceptionDummy implements IElevator {
	public ThrowExceptionAt mExceptionAt = ThrowExceptionAt.None;
	public int mClockTick = 0;
	
	@Override
	public int getCommittedDirection(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.CommittedDirection)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorAccel(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorAcceleration)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public boolean getElevatorButton(int elevatorNumber, int floor) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorButton)
//			throw new RemoteException();
		return false;
	}

	@Override
	public int getElevatorDoorStatus(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorDoorStatus)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorFloor(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorFloor)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorNum() throws RemoteException {
		if (mExceptionAt == ThrowExceptionAt.ElevatorNum)
			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorPosition(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorPosition)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorSpeed(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorSpeed)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorWeight(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorWeight)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getElevatorCapacity(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ElevatorCapacity)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public boolean getFloorButtonDown(int floor) throws RemoteException {
		if (mExceptionAt == ThrowExceptionAt.FloorButtonDown)
			throw new RemoteException();
		return false;
	}

	@Override
	public boolean getFloorButtonUp(int floor) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.FloorButtonUp)
//			throw new RemoteException();
		return false;
	}

	@Override
	public int getFloorHeight() throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.FloorHeight)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public int getFloorNum() throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.FloorNum)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public boolean getServicesFloors(int elevatorNumber, int floor) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.ServicesFloor)
//			throw new RemoteException();
		return false;
	}

	@Override
	public int getTarget(int elevatorNumber) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.Target)
//			throw new RemoteException();
		return 0;
	}

	@Override
	public void setCommittedDirection(int elevatorNumber, int direction) throws RemoteException {
		if (mExceptionAt == ThrowExceptionAt.setCommittedDirection)
			throw new RemoteException();
	}

	@Override
	public void setServicesFloors(int elevatorNumber, int floor, boolean service) throws RemoteException {
//		if (mExceptionAt == ThrowExceptionAt.setServicesFloor)
//			throw new RemoteException();
	}

	@Override
	public void setTarget(int elevatorNumber, int target) throws RemoteException, Exception {
		if (mExceptionAt == ThrowExceptionAt.setTarget)
			throw new RemoteException();
	}

	@Override
	public long getClockTick() throws RemoteException {
		if (mExceptionAt == ThrowExceptionAt.ClockTick)
			throw new RemoteException();
		return mClockTick;
	}

}
