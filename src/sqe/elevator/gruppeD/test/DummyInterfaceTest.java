/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.DummyInterface;
import sqelevator.IElevator;

public class DummyInterfaceTest {
	DummyInterface mDummyInterface;
	
	@Before
	public void setUp() throws Exception {
		mDummyInterface = new DummyInterface(10);
	}
	
	@Test
	public void testUpDown() throws Exception {
		assertEquals(0, mDummyInterface.getElevatorFloor(0));
		
		// Calls to getElevatorPosition() to force an Update and make the elevator arrive
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		// Move up
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_UP);
		mDummyInterface.setTarget(0, mDummyInterface.getFloorNum() - 1);
		
		// Force Update
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		// Assert elevator is moving
		assertEquals(1, mDummyInterface.getElevatorAccel(0));
		assertEquals(10, mDummyInterface.getElevatorSpeed(0));
		
		// Wait for elevator to arrive
		for(int i = 0; i < mDummyInterface.getFloorNum() * 10; i++) {
			Thread.sleep(10);
			mDummyInterface.getElevatorPosition(0);
		}
		
		assertEquals(mDummyInterface.getFloorNum() - 1, mDummyInterface.getElevatorFloor(0));
		
		// Move down
		mDummyInterface.setTarget(0, 0);
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_DOWN);
		
		// Force Update
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		// Assert elevator is moving
		assertEquals(-1, mDummyInterface.getElevatorAccel(0));
		assertEquals(-10, mDummyInterface.getElevatorSpeed(0));
		
		for(int i = 0; i < mDummyInterface.getFloorNum() * 10; i++) {
			Thread.sleep(11);
			mDummyInterface.getElevatorPosition(0);
		}
		
		assertEquals(0, mDummyInterface.getElevatorFloor(0));
	}
	
	@Test
	public void testDummyInterface() throws RemoteException {
		DummyInterface dummyInterface = new DummyInterface(1000);
		assertEquals(1000, dummyInterface.getClockTick());
	}

	@Test
	public void testGetCommittedDirection() throws RemoteException {
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_UP);
		assertEquals(IElevator.ELEVATOR_DIRECTION_UP, mDummyInterface.getCommittedDirection(0));
	}

	@Test
	public void testGetElevatorAccel() throws Exception {
		// Calls to getElevatorPosition() to force an Update and make the elevator arrive
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_UP);
		mDummyInterface.setTarget(0, mDummyInterface.getFloorNum() - 1);
		
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		assertEquals(1, mDummyInterface.getElevatorAccel(0));
	}

	@Test
	public void testGetElevatorButton() throws RemoteException {
		assertFalse(mDummyInterface.getElevatorButton(0, 0));
	}

	@Test
	public void testGetElevatorDoorStatus() throws RemoteException {
		assertEquals(IElevator.ELEVATOR_DOORS_CLOSED, mDummyInterface.getElevatorDoorStatus(0));
	}

	@Test
	public void testGetElevatorFloor() throws Exception {
		assertEquals(0, mDummyInterface.getElevatorFloor(0));
		
		// Calls to getElevatorPosition() to force an Update and make the elevator arrive
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_UP);
		mDummyInterface.setTarget(0, mDummyInterface.getFloorNum() - 1);
		
		for(int i = 0; i < mDummyInterface.getFloorNum() * 10; i++) {
			Thread.sleep(11);
			mDummyInterface.getElevatorPosition(0);
		}
		
		assertEquals(mDummyInterface.getFloorNum() - 1, mDummyInterface.getElevatorFloor(0));
	}

	@Test
	public void testGetElevatorNum() throws RemoteException {
		// Random number between 1 and 5
		assertTrue(mDummyInterface.getElevatorNum() > 0);
		assertTrue(mDummyInterface.getElevatorNum() <= 5);
	}

	@Test
	public void testGetElevatorPosition() throws RemoteException {
		// Can’t be tested, filled with random numbers
		assertEquals(0, mDummyInterface.getElevatorPosition(0));
	}

	@Test
	public void testGetElevatorSpeed() throws RemoteException {
		assertEquals(0, mDummyInterface.getElevatorSpeed(0));
	}

	@Test
	public void testGetElevatorWeight() throws RemoteException {
		// Can’t be tested, filled with random numbers
		assertEquals(0, mDummyInterface.getElevatorWeight(0));
	}

	@Test
	public void testGetFloorButtonDown() throws RemoteException {
		// Can’t be tested, filled with random numbers
		assertFalse(mDummyInterface.getFloorButtonDown(0));
	}

	@Test
	public void testGetFloorButtonUp() throws RemoteException {
		// Can’t be tested, filled with random numbers
		assertFalse(mDummyInterface.getFloorButtonUp(0));
	}

	@Test
	public void testGetFloorHeight() throws RemoteException {
		// Random number between 1 and 5
		assertTrue(mDummyInterface.getFloorHeight() > 0);
		assertTrue(mDummyInterface.getFloorHeight() <= 5);
	}

	@Test
	public void testGetFloorNum() throws RemoteException {
		// Random number between 1 and 5
		assertTrue(mDummyInterface.getFloorNum() >= 5);
		assertTrue(mDummyInterface.getFloorNum() <= 20);
	}

	@Test
	public void testGetServicesFloors() throws RemoteException {
		mDummyInterface.setServicesFloors(0, 0, true);
		assertTrue(mDummyInterface.getServicesFloors(0, 0));
		
		mDummyInterface.setServicesFloors(0, 0, false);
		assertFalse(mDummyInterface.getServicesFloors(0, 0));
	}

	@Test
	public void testGetTarget() throws Exception {
		// Calls to getElevatorPosition() to force an Update and make the elevator arrive. Only then is a new target possible.
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		mDummyInterface.setTarget(0, mDummyInterface.getFloorNum() - 1);
		assertEquals(mDummyInterface.getFloorNum() - 1, mDummyInterface.getTarget(0));
	}

	@Test
	public void testSetCommittedDirection() throws RemoteException {
		mDummyInterface.setCommittedDirection(0, IElevator.ELEVATOR_DIRECTION_UP);
		assertEquals(IElevator.ELEVATOR_DIRECTION_UP, mDummyInterface.getCommittedDirection(0));
	}

	@Test
	public void testSetServicesFloors() throws RemoteException {
		mDummyInterface.setServicesFloors(0, 0, true);
		assertTrue(mDummyInterface.getServicesFloors(0, 0));
		
		mDummyInterface.setServicesFloors(0, 0, false);
		assertFalse(mDummyInterface.getServicesFloors(0, 0));
	}

	@Test
	public void testSetTarget() throws RemoteException, Exception {
		// Calls to getElevatorPosition() to force an Update and make the elevator arrive. Only then is a new target possible.
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		Thread.sleep(15);
		mDummyInterface.getElevatorPosition(0);
		
		mDummyInterface.setTarget(0, mDummyInterface.getFloorNum() - 1);
		assertEquals(mDummyInterface.getFloorNum() - 1, mDummyInterface.getTarget(0));
		
		boolean exceptionThrown = false;
		try {
			mDummyInterface.setTarget(0, mDummyInterface.getFloorNum());
		} catch (Exception e) {
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
	}

	@Test
	public void testGetClockTick() throws RemoteException {
		DummyInterface dummyInterface = new DummyInterface(123);
		assertEquals(123, dummyInterface.getClockTick());
	}

	@Test
	public void testGetElevatorCapacity() throws RemoteException {
		assertEquals(0, mDummyInterface.getElevatorCapacity(0));
	}

}
