/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.ElevatorControlCenter;
import sqe.elevator.gruppeD.data.Mode;
import sqelevator.IElevator;

public class ElevatorControlCenterTest {
	private ElevatorControlCenter ecc;

	@Before
	public void setUp() {
		IElevator dummy = new IElevatorEmptyDummy();
		
		ecc = new ElevatorControlCenter(dummy, false, "");
	}

	@Test
	public void testElevatorControlCenter() throws InterruptedException {	
		ecc.PollElevatorInterface();
		ecc.UpdateGui();
		ecc.Control();
		Thread.sleep(1000);
		
		ecc.ModeListener(Mode.AUTOMATIC);
		assertEquals(Mode.AUTOMATIC, ecc.testAccessMode());
		ecc.ModeListener(Mode.MANUAL);
		assertEquals(Mode.MANUAL, ecc.testAccessMode());
		
		ecc.TargetListener(0, 0);
		assertEquals(0, ecc.testAcccessTarget(0));
		ecc.TargetListener(1, 0);
		assertEquals(1, ecc.testAcccessTarget(0));
		
		ecc.PollElevatorInterface();
		ecc.UpdateGui();
		ecc.Control();
		Thread.sleep(1000);
		
		ecc.PollElevatorInterface();
		ecc.UpdateGui();
		ecc.Control();
		Thread.sleep(1000);
		
		ecc.PollElevatorInterface();
		ecc.UpdateGui();
		ecc.Control();
		Thread.sleep(1000);
		
		ecc.TargetListener(0, 0);
	}

}
