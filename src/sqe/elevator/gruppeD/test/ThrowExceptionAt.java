/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

public enum ThrowExceptionAt {
	None, //CommittedDirection, ElevatorAcceleration, ElevatorButton, ElevatorDoorStatus, ElevatorFloor,
	ElevatorNum, //ElevatorPosition, ElevatorSpeed, ElevatorWeight, ElevatorCapacity,
	FloorButtonDown, //FloorButtonUp, FloorHeight, FloorNum, ServicesFloor, Target,
	setCommittedDirection, setTarget, //setServicesFloor,
	ClockTick
}
