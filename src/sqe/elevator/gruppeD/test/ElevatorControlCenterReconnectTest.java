/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import sqe.elevator.gruppeD.ElevatorControlCenter;
import sqelevator.IElevator;

public class ElevatorControlCenterReconnectTest {
	private ElevatorControlCenter ecc;
	private IElevator dummy;

	@Before
	public void setUp() {
		dummy = new IElevatorEmptyDummy();
	}

	@Test
	public void testWrongUrl()  {
		ecc = new ElevatorControlCenter(dummy, true, "abc.a#d");
		
		ecc.ConnectionLost();
		assertEquals("URL malformed, no connection to elevator available.", ecc.testAccessLastError());
	}
	
	@Test
	public void testRemoteException() {
		ecc = new ElevatorControlCenter(dummy, true, "rmi://localhost/ElevatorSim");
		
		ecc.ConnectionSyncLost();
		assertEquals("Connection failed.", ecc.testAccessLastError());
	}
}
