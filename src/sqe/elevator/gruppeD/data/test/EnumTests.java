/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data.test;

import org.junit.Test;

import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.data.ElevatorState;
import sqe.elevator.gruppeD.data.Mode;

/**
 * Workaround to get 100% coverage for enums. 
 * Enums generate hidden byte code which makes a call to values() and valueOf() necessary.
 *
 */
public class EnumTests {

	@Test
	public void test() {
		Mode.values();
		Mode.valueOf("MANUAL");
		
		ElevatorState.values();
		ElevatorState.valueOf("RUNNING");
		
		ButtonState.values();
		ButtonState.valueOf("DISABLED");
	}

}
