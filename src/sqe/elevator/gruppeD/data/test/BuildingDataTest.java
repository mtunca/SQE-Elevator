/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data.test;

import static org.junit.Assert.*;

import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;

public class BuildingDataTest {
	private BuildingData data;

	@Test
	public void testCtorWithoutArgs(){
		data = new BuildingData();
		
		assertEquals(1, data.mFloorHeight);
		assertEquals(1, data.mNumElevators);
		assertEquals(1, data.mNumFloors);
	}

	@Test
	public void testCtorWitArgs(){
		data = new BuildingData(1, 2, 3);
		
		assertEquals(1, data.mFloorHeight);
		assertEquals(2, data.mNumElevators);
		assertEquals(3, data.mNumFloors);
	}
	
	@Test
	public void testOutOfBounds(){
		data = new BuildingData(0, 2, 3);
		
		assertEquals(1, data.mFloorHeight);
		assertEquals(1, data.mNumElevators);
		assertEquals(1, data.mNumFloors);

		data = new BuildingData(1, 0, 3);
		
		assertEquals(1, data.mFloorHeight);
		assertEquals(1, data.mNumElevators);
		assertEquals(1, data.mNumFloors);
		
		data = new BuildingData(1, 2, 0);
		
		assertEquals(1, data.mFloorHeight);
		assertEquals(1, data.mNumElevators);
		assertEquals(1, data.mNumFloors);
	}
}
