/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data.test;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Test;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.data.DataWrapper;
import sqe.elevator.gruppeD.data.ElevatorData;

public class DataWrapperTest {

	@Test
	public void testDataWrapper() throws Exception {
		BuildingData building = new BuildingData(3, 3, 3);
		
		DataWrapper dataWrapper = new DataWrapper(building);
		assertEquals(building, dataWrapper.mBuilding);
	}

	@Test
	public void testSetElevatorData() throws Exception {
		BuildingData building = new BuildingData(3, 3, 3);
		
		DataWrapper dataWrapper = new DataWrapper(building);
		
		ElevatorData elevator0 = new ElevatorData();
		
		dataWrapper.SetElevatorData(0, elevator0);
		assertEquals(elevator0, dataWrapper.mElevatorList[0]);
		
		ElevatorData elevator1 = new ElevatorData();
		dataWrapper.SetElevatorData(1, elevator1);
		assertEquals(elevator1, dataWrapper.mElevatorList[1]);
		assertEquals(elevator0, dataWrapper.mElevatorList[0]);
		
		dataWrapper.SetElevatorData(-1, elevator0);
		assertEquals(elevator1, dataWrapper.mElevatorList[1]);
		assertEquals(elevator0, dataWrapper.mElevatorList[0]);
		
		dataWrapper.SetElevatorData(3, elevator0);
		assertEquals(elevator1, dataWrapper.mElevatorList[1]);
		assertEquals(elevator0, dataWrapper.mElevatorList[0]);
	}

	@Test
	public void testSetElevatorBtnsPressed() {
		BuildingData building = new BuildingData(3, 3, 3);
		
		DataWrapper dataWrapper = new DataWrapper(building);
		
		Vector<Integer> buttons = new Vector<Integer>();
		buttons.add(1);
		buttons.add(2);
		dataWrapper.SetElevatorBtnsPressed(0, buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][1]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][2]);
		
		dataWrapper.SetElevatorBtnsPressed(0, buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][1]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][2]);
		
		dataWrapper.SetElevatorBtnsPressed(-1, buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][1]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][2]);
		
		dataWrapper.SetElevatorBtnsPressed(3, buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][1]);
		assertEquals(ButtonState.ON, dataWrapper.mElevatorStopState[0][2]);
	}

	@Test
	public void testSetFloorBtnsDownPressed() throws Exception {
		BuildingData building = new BuildingData(3, 3, 3);
		
		DataWrapper dataWrapper = new DataWrapper(building);
		
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsDownState[0]);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsDownState[1]);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsDownState[2]);
		
		Vector<Integer> buttons = new Vector<Integer>();
		buttons.add(1);
		buttons.add(2);
		
		dataWrapper.SetFloorBtnsDownPressed(buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsDownState[0]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsDownState[1]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsDownState[2]);
		
		dataWrapper.SetFloorBtnsDownPressed(buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsDownState[0]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsDownState[1]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsDownState[2]);
	}

	@Test
	public void testSetFloorBtnsUpPressed() throws Exception {
		BuildingData building = new BuildingData(3, 3, 3);
		
		DataWrapper dataWrapper = new DataWrapper(building);
		
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[0]);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[1]);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[2]);
		
		Vector<Integer> buttons = new Vector<Integer>();
		buttons.add(1);
		buttons.add(2);
		
		dataWrapper.SetFloorBtnsUpPressed(buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[0]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsUpState[1]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsUpState[2]);
		
		dataWrapper.SetFloorBtnsUpPressed(buttons);
		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[0]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsUpState[1]);
		assertEquals(ButtonState.ON, dataWrapper.mFloorBtnsUpState[2]);
	}

//	@Test
//	public void testSetElevatorFloorsNotServiced() throws Exception {
//		BuildingData building = new BuildingData(3, 3, 3);
//		
//		DataWrapper dataWrapper = new DataWrapper(building);
//		
//		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[0]);
//		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[1]);
//		assertEquals(ButtonState.OFF, dataWrapper.mFloorBtnsUpState[2]);
//		
//		Vector<Integer> buttons = new Vector<Integer>();
//		buttons.add(1);
//		buttons.add(2);
//		
//		dataWrapper.SetElevatorFloorsNotServiced(0, buttons);
//		
//		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
//		assertEquals(ButtonState.DISABLED, dataWrapper.mElevatorStopState[0][1]);
//		assertEquals(ButtonState.DISABLED, dataWrapper.mElevatorStopState[0][2]);
//		
//		dataWrapper.SetElevatorFloorsNotServiced(0, buttons);
//		
//		assertEquals(ButtonState.OFF, dataWrapper.mElevatorStopState[0][0]);
//		assertEquals(ButtonState.DISABLED, dataWrapper.mElevatorStopState[0][1]);
//		assertEquals(ButtonState.DISABLED, dataWrapper.mElevatorStopState[0][2]);
//		
//		dataWrapper.SetElevatorFloorsNotServiced(-1, buttons);
//		dataWrapper.SetElevatorFloorsNotServiced(3, buttons);
//	}
}
