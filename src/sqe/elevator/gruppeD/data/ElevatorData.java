/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data;

import sqelevator.IElevator;

/**
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 */
public class ElevatorData {
	public int mAcceleration;
	public int mCommittedDirection;
	public int mDoorStatus;
	public int mFloor;
	public int mPosition;
	public int mSpeed;
	public int mTarget;
	public int mWeight;
	
	public ElevatorData () {
		mAcceleration = 0;
		mCommittedDirection = IElevator.ELEVATOR_DIRECTION_UNCOMMITTED;
		mDoorStatus = IElevator.ELEVATOR_DOORS_CLOSED;
		mFloor = 0;
		mPosition = 0;
		mSpeed = 0;
		mTarget = 0;
		mWeight = 0;
	}
}
