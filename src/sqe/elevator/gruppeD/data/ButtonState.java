/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * description of the supported states of the buttons
 */
public enum ButtonState {
	ON, OFF, DISABLED
}
