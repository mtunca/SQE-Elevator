/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data;

import java.util.Arrays;
import java.util.Vector;

public class DataWrapper {

	public BuildingData 		mBuilding;
	
	public ElevatorData[] 			mElevatorList;
	public ButtonState[] 		mFloorBtnsDownState;
	public ButtonState[] 		mFloorBtnsUpState;
	public ButtonState[][]	 	mElevatorStopState;
	
	public DataWrapper (BuildingData building) {
		mBuilding = building;

		mElevatorList = new ElevatorData[building.mNumElevators];
		mFloorBtnsDownState = new ButtonState[building.mNumFloors];
		Arrays.fill(mFloorBtnsDownState, ButtonState.OFF);
		mFloorBtnsUpState = new ButtonState[building.mNumFloors];
		Arrays.fill(mFloorBtnsUpState, ButtonState.OFF);
		mElevatorStopState = new ButtonState[building.mNumElevators][building.mNumFloors];
		for(int i = 0; i < building.mNumElevators; i++) {
			Arrays.fill(mElevatorStopState[i], ButtonState.OFF);
		}
	}
	/**
	 * method to set for a given id the data for an elevator in the elevator list
	 * @param id of the elevator to be set
	 * @param data elevator data assigned to the elevatorlist for a given id
	 * @throws Exception
	 */
	public void SetElevatorData (int id, ElevatorData data) {
		if ((id < 0) || mBuilding.mNumElevators <= id) {
			return;
		}
		
		mElevatorList[id] = data;
	}
	

	/**
	 * setting the states of the pressed buttons
	 * @param id is used to map the buttons states for a specific elevator in the elevator list
	 * @param btns marks the positions of all pressed buttons
	 * @throws Exception
	 */
	public void SetElevatorBtnsPressed (int id, Vector<Integer> btns) {
		if ((id < 0) || mBuilding.mNumElevators <= id) {
			return;
		}
		
		// reset all buttons
		for (int i=0; i<mBuilding.mNumFloors; ++i) {
			if (mElevatorStopState[id][i] == ButtonState.ON) {
				mElevatorStopState[id][i] = ButtonState.OFF;
			}
		}
		
		// set buttons
		for (int i=0; i<btns.size(); ++i) {
			mElevatorStopState[id][btns.get(i)] = ButtonState.ON;
		}
	}
	
//	public void SetElevatorFloorsNotServiced (int id, Vector<Integer> btns) {
//		if ((id < 0) || mBuilding.mNumElevators <= id) {
//			return;
//		}
//		
//		// reset all floors
//		for (int i=0; i<mBuilding.mNumFloors; ++i) {
//			if (mElevatorStopState[id][i] == ButtonState.DISABLED) {
//				mElevatorStopState[id][i] = ButtonState.OFF;
//			}
//		}
//		
//		// set floors
//		for (int i=0; i<btns.size(); ++i) {
//			mElevatorStopState[id][btns.get(i)] = ButtonState.DISABLED;
//		}
//	}

	/**
	 * setting the states of the pressed down-buttons
	 * @param id is used to map the down-buttons states for a specific elevator in the elevator list
	 * @param btns marks the positions of all pressed buttons
	 * @throws Exception
	 */
	public void SetFloorBtnsDownPressed (Vector<Integer> btns) {
		// reset all down buttons
		for (int i=0; i<mBuilding.mNumFloors; ++i) {
			if (mFloorBtnsDownState[i] == ButtonState.ON) {
				mFloorBtnsDownState[i] = ButtonState.OFF;
			}
		}
		
		// set floors
		for (int i=0; i<btns.size(); ++i) {
			mFloorBtnsDownState[btns.get(i)] = ButtonState.ON;
		}
	}
	
	public void SetFloorBtnsUpPressed (Vector<Integer> btns) {
		// reset all down buttons
		for (int i=0; i<mBuilding.mNumFloors; ++i) {
			if (mFloorBtnsUpState[i] == ButtonState.ON) {
				mFloorBtnsUpState[i] = ButtonState.OFF;
			}
		}
		
		// set floors
		for (int i=0; i<btns.size(); ++i) {
			mFloorBtnsUpState[btns.get(i)] = ButtonState.ON;
		}
	}
}
