/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD.data;

/**
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 */
public class BuildingData {
	public int mFloorHeight;
	public int mNumElevators;
	public int mNumFloors;
	
	public BuildingData () {
		mFloorHeight = 1;
		mNumElevators = 1;
		mNumFloors = 1;
	}
	
	public BuildingData (int floorHeight, int numElev, int numFloors) {
		if (floorHeight > 0 && numElev > 0 && numFloors > 0) {
			mFloorHeight = floorHeight;
			mNumElevators = numElev;
			mNumFloors = numFloors;
		}
		else {
			mFloorHeight = 1;
			mNumElevators = 1;
			mNumFloors = 1;
		}
	}
}
