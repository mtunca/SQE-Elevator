/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Vector;

import sqe.elevator.gruppeD.data.DataWrapper;
import sqe.elevator.gruppeD.data.ElevatorData;
import sqe.elevator.gruppeD.data.Mode;
import sqe.elevator.gruppeD.gui.ElevatorGui;
import sqe.elevator.gruppeD.gui.IModeTargetListener;
import sqelevator.IElevator;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * This class is the control section of the MVC pattern. Which contains the
 * mGui object representing the view and the mElevatorControl object representing model, which
 * encapsulates business logic (algorithms) and data. This class maps the model to the view.
 */
public class ElevatorControlCenter implements IModeTargetListener, IElevatorConnection {
	private ElevatorInterfaceWrapper mIElevator;
	private boolean					 mIElevatorIsConnectable;
	private String					 mIElevatorAddress;
	
	private DataWrapper				 mData;
	private ElevatorGui 			 mGui;
	private AutomaticControl 		 mAutomatic;

	private int[] 					 mElevatorTarget; // Used for communication with GUI
	private Mode					 mMode; // Used for communication with GUI
	private String					 mLastErrorMessage;
	
	public static int mPollingInterval = 300;
	/**
	 * constructor of the control class
	 * @throws Exception 
	 */
	public ElevatorControlCenter(IElevator elevatorIF, boolean isConnectable, String rmiAddress) {
		mIElevator = new ElevatorInterfaceWrapper(elevatorIF, this);
		mIElevatorIsConnectable = isConnectable;
		mIElevatorAddress = rmiAddress;
		
		mData = new DataWrapper(mIElevator.GetBuilding());
		mElevatorTarget = new int[mIElevator.GetBuilding().mNumElevators];
		Arrays.fill(mElevatorTarget, 0);
		
		mAutomatic = new AutomaticControl(mData);
		mGui = new ElevatorGui(mData, this); // create the GUI
		
		mGui.setVisible(true);
	}
	
	public void Run () {
		while (true) {
			PollElevatorInterface();
			Control();
			UpdateGui();
			
			try {
				Thread.sleep(mPollingInterval);
			}
			catch (InterruptedException e) {
				mLastErrorMessage = "Polling Error.";
				mGui.DisplayError(mLastErrorMessage);
			}
		}
	}
	
	/**
	 * get the current states of the elevators and their buttons
	 * @throws Exception 
	 */
	public void PollElevatorInterface() {
		ElevatorData[]		elevators;
		Vector<Integer>  	upButtons;
		Vector<Integer>  	downButtons;
		Vector<Vector<Integer>>	stopButtons = new Vector<Vector<Integer>>();
		
		elevators = mIElevator.GetElevators();
		for (int elevatorId=0; elevatorId<mData.mBuilding.mNumElevators; elevatorId++) {
			stopButtons.add(mIElevator.GetElevatorBtnsPressed(elevatorId));
		}
		upButtons = mIElevator.GetFloorBtnsUpPressed();
		downButtons = mIElevator.GetFloorBtnsDownPressed();
		
		for(int i = 0; i < elevators.length; i++) {
			mData.SetElevatorData(i, elevators[i]);
			mData.SetElevatorBtnsPressed(i, stopButtons.get(i));
		}
		mData.SetFloorBtnsUpPressed(upButtons);
		mData.SetFloorBtnsDownPressed(downButtons);
	}
	
	/**
	 * after getting the pressed buttons states, update the controller and the gui
	 * @throws Exception
	 */
	public void UpdateGui() {
		mGui.Update();
	}
	
	/**
	 * scheduling method for controlling the elevators. The two modes, automatic and manual, are handled here.
	 * @throws Exception 
	 */
	public void Control() {
		for (int i=0; i<mData.mElevatorList.length; ++i) {
			if(mData.mElevatorList[i] != null) {
				if(mMode == Mode.AUTOMATIC) {
					mElevatorTarget[i] = mAutomatic.GetTarget(i);
				}


				if(mData.mElevatorList[i].mDoorStatus == IElevator.ELEVATOR_DOORS_OPEN) {
					mIElevator.SetElevatorTarget(i, mElevatorTarget[i]);
					if(mData.mElevatorList[i].mFloor < mElevatorTarget[i]) {
						mIElevator.SetElevatorDirection(i, IElevator.ELEVATOR_DIRECTION_UP);
					} else if(mData.mElevatorList[i].mFloor > mElevatorTarget[i]) {
						mIElevator.SetElevatorDirection(i, IElevator.ELEVATOR_DIRECTION_DOWN);
					} else {
						mIElevator.SetElevatorDirection(i, IElevator.ELEVATOR_DIRECTION_UNCOMMITTED);
					}
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see sqe.elevator.gruppeD.IModeTargetListener#ModeListener(sqe.elevator.gruppeD.data.Mode)
	 */
	@Override
	public void ModeListener(Mode mode) {
		mMode = mode;
		Control();
	}
	
	/* (non-Javadoc)
	 * @see sqe.elevator.gruppeD.IModeTargetListener#TargetListener(int, int)
	 */
	@Override
	public void TargetListener(int floor, int elevator) {
		//System.out.println("EC get target: " + floor + " on " + elevator);
		mElevatorTarget[elevator] = floor;
		Control();
	}

	@Override
	public void ConnectionLost() {
		Reconnect(3);
	}

	@Override
	public void ConnectionSyncLost() {
		Reconnect(1);		
	}

	private void Reconnect (int maxAttempts) {
		IElevator ielev;
		if (mIElevatorIsConnectable) {
			// try reconnecting
			for (int i=0; i<maxAttempts; i++) {
				if (mIElevatorIsConnectable) { // url or not bound exception
					try {
						ielev = (IElevator) Naming.lookup(mIElevatorAddress);
						
						mIElevator.SetInterface(ielev, true); // if successful reconnect interface
						break;
					}
					catch (MalformedURLException e) {
						// url is wrong
						mIElevatorIsConnectable = false;
						mLastErrorMessage = "URL malformed, no connection to elevator available.";
						mGui.DisplayError(mLastErrorMessage);
					}
					catch (RemoteException e) {
						if (i == maxAttempts-1) {
							mIElevatorIsConnectable = false;
							mLastErrorMessage = "Connection failed.";
							mGui.DisplayError(mLastErrorMessage);
						}
					}
					catch (NotBoundException e) {
						mIElevatorIsConnectable = false;
						mLastErrorMessage = "RMI Service not available.";
						mGui.DisplayError(mLastErrorMessage);
					}
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * Test methods to access private members
	 */	
	public int testAcccessTarget(int elevator) {
		return mElevatorTarget[elevator];
	}
	
	public Mode testAccessMode() {
		return mMode;
	}
	
	public String testAccessLastError () {
		String last = mLastErrorMessage;
		mLastErrorMessage = "";
		return last;
	}
}
