/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import sqelevator.IElevator;

public class Main {
	public static void main(String[] args) throws Exception {
		
		boolean isDummy = false;
		String rmi = "rmi://localhost/ElevatorSim";
		IElevator ielev = new DummyInterface(ElevatorControlCenter.mPollingInterval);
		
		try {
			if(!isDummy) {
				ielev = (IElevator) Naming.lookup(rmi);
			}
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (RemoteException e) {
			e.printStackTrace();
		}
		catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		ElevatorControlCenter ecc = new ElevatorControlCenter(ielev, !isDummy, rmi);
		ecc.Run();
	}
	
}
