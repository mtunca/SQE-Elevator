/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD;

import java.rmi.RemoteException;
import java.util.Vector;

import sqe.elevator.gruppeD.data.BuildingData;
import sqe.elevator.gruppeD.data.ElevatorData;
import sqelevator.IElevator;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * wrapper class for the given IElevator interface.
 * as an option a dummy interface is used for test data.
 * 
 */
public class ElevatorInterfaceWrapper {
	private IElevator mElevatorInterface;
	private IElevatorConnection mElevatorConnection;
	private boolean mIsInterfaceActive;
	private BuildingData mBuilding;
	private ElevatorData[] mElevators;
	private Vector<Vector<Integer>> mStopBtns;
	private Vector<Integer> mFloorUpBtns;
	private Vector<Integer> mFloorDownBtns;
	private long mLastClockTick = -1;
	private boolean mIsEmpty;
	
	/**
	 * constructor of the wrapper
	 * @param isDummy use the dummy-object if it is implemented.
	 * @throws Exception 
	 */
	public ElevatorInterfaceWrapper(IElevator elevatorInterface, IElevatorConnection elevatorConn) {
		mElevatorInterface = elevatorInterface;
		mElevatorConnection = elevatorConn;
		mIsInterfaceActive = true;
		
		UpdateBuildingData();
		CreateEmptyData();
	}
	
	public void SetInterface(IElevator elevatorInterface, boolean isActive) {
		mElevatorInterface = elevatorInterface;
		mIsInterfaceActive = isActive;
	}
	
	public BuildingData GetBuilding() {
		return mBuilding;
	}
	
	private void UpdateBuildingData() {
		int numElevator = 1;
		int numFloors = 1;
		int floorHeight = 1;
		
		try {
			numElevator = mElevatorInterface.getElevatorNum();
			numFloors = mElevatorInterface.getFloorNum();
			floorHeight = mElevatorInterface.getFloorHeight();
		} catch (RemoteException e) {
			mElevatorConnection.ConnectionLost();
			mIsInterfaceActive = false;
		}
		
		mBuilding = new BuildingData(floorHeight, numElevator, numFloors);
	}
	
	private void CreateEmptyData () {
		if (!mIsEmpty) {
			mElevators = new ElevatorData[mBuilding.mNumElevators];
			mStopBtns = new Vector<Vector<Integer>>();
			for (int elevid = 0; elevid < mBuilding.mNumElevators; elevid++) {
				mElevators[elevid] = new ElevatorData();
				mStopBtns.add(new Vector<Integer>());
			}
			mFloorDownBtns = new Vector<Integer>();
			mFloorUpBtns = new Vector<Integer>();
			mIsEmpty = true;
		}
	}
	
	private void Update(int cnt) {
		if (!mIsInterfaceActive || cnt >= 5) {
			CreateEmptyData();
			if (cnt >= 5) {
				mElevatorConnection.ConnectionSyncLost();
			}
			mIsInterfaceActive = false;
			return;
		}
		
		long clockTick = 0;
		
		try {
			clockTick = mElevatorInterface.getClockTick();
		}
		catch (RemoteException e) {
			mElevatorConnection.ConnectionLost();
			mIsInterfaceActive = false;
			return;
		}
		
		if (clockTick <= mLastClockTick) {
			return; // no update necessary
		}
		
		mElevators = new ElevatorData[mBuilding.mNumElevators];
		mStopBtns = new Vector<Vector<Integer>>();		

		try {
			for (int elevid = 0; elevid < mBuilding.mNumElevators; elevid++) {
				// get elevator data
				mElevators[elevid] = new ElevatorData();
				mElevators[elevid].mCommittedDirection = mElevatorInterface.getCommittedDirection(elevid);
				mElevators[elevid].mAcceleration	   = mElevatorInterface.getElevatorAccel(elevid);
				mElevators[elevid].mDoorStatus 		   = mElevatorInterface.getElevatorDoorStatus(elevid);
				mElevators[elevid].mFloor			   = mElevatorInterface.getElevatorFloor(elevid);
				mElevators[elevid].mPosition 		   = mElevatorInterface.getElevatorPosition(elevid);
				mElevators[elevid].mSpeed 			   = mElevatorInterface.getElevatorSpeed(elevid);
				mElevators[elevid].mWeight 			   = mElevatorInterface.getElevatorWeight(elevid);
				mElevators[elevid].mTarget   		   = mElevatorInterface.getTarget(elevid);
	
				// get elevator stop btns
				Vector<Integer> eStopBtn = new Vector<Integer>();
				for (int floorCounter = 0; floorCounter < mBuilding.mNumFloors; floorCounter++) {
					if (mElevatorInterface.getElevatorButton(elevid, floorCounter)) {
						eStopBtn.add(floorCounter);
					}
				}
				mStopBtns.add(eStopBtn);
			}
	
			mFloorDownBtns = new Vector<Integer>();
			mFloorUpBtns = new Vector<Integer>();
			for(int floorCounter = 0; floorCounter < mBuilding.mNumFloors; floorCounter++) {
				if (mElevatorInterface.getFloorButtonDown(floorCounter)) {
					mFloorDownBtns.add(floorCounter);
				}
				
				if (mElevatorInterface.getFloorButtonUp(floorCounter)) {
					mFloorUpBtns.add(floorCounter);
				}
			}
			
			if (clockTick != mElevatorInterface.getClockTick()) {
				Update(cnt+1);
			}
			mIsEmpty = false;
			mLastClockTick = clockTick;
		}
		catch (RemoteException e) {
			mElevatorConnection.ConnectionLost();
			mIsInterfaceActive = false;
			return;
		}
		
	}
	
	public ElevatorData[] GetElevators () {
		Update(1);
		return mElevators;
	}
	
	public Vector<Integer> GetElevatorBtnsPressed (int elevatorId) {
		Update(1);
		return mStopBtns.get(elevatorId);
	}
	
	public Vector<Integer> GetFloorBtnsUpPressed() {
		Update(1);
		return mFloorUpBtns;
	}
	
	public Vector<Integer> GetFloorBtnsDownPressed() {
		Update(1);
		return mFloorDownBtns;
	}
	
//	public void SetElevatorServicedFloors(int elevator, Vector<Integer> floors) {
//		try {
//			for(int i = 0; i < mElevatorInterface.getFloorNum(); i++) {
//				if(floors.contains(i)) {
//					mElevatorInterface.setServicesFloors(elevator, i, true);
//				} else {
//					mElevatorInterface.setServicesFloors(elevator, i, false);
//				}
//			}
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		}
//	}
	
	public void SetElevatorTarget (int elevator, int targetFloor) {
		if ((elevator < 0 || mBuilding.mNumElevators <= elevator) || (targetFloor < 0 || mBuilding.mNumFloors <= targetFloor)) {
			return;
		}
		
		try {
			mElevatorInterface.setTarget(elevator, targetFloor);
		}
		catch (RemoteException e) {
			mElevatorConnection.ConnectionLost();
			mIsInterfaceActive = false;
			return;
		}
		catch (Exception e) {
			// this exception should not happen - if it does, the program has an error
			e.printStackTrace();
		}
	}
	
	public void SetElevatorDirection(int elevator, int direction) {
		try {
			mElevatorInterface.setCommittedDirection(elevator, direction);
		}
		catch (RemoteException e) {
			mElevatorConnection.ConnectionLost();
			mIsInterfaceActive = false;
			return;
		}
	}
}
