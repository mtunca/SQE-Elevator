/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD;

import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.data.DataWrapper;
import sqelevator.IElevator;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * This class builds the main logic for all elevators.
 * On construction a wrapper object for IElevator is passed, from where
 * generated test data is received. Hence, the changed
 * state of the elevators is updated by calling the setter functions of the wrapper class.
 */
public class AutomaticControl {
	private DataWrapper		mData;
	
	/**
	 * 
	 * @param building contains information about the supported properties of the elevators
	 * @param elevatorInterfaceWrapper an object which implements the IElevator interface controlling the whole elevator system
	 */
	public AutomaticControl (DataWrapper data) {
		mData = data;
	}
	
	/**
	 * scheduling method for automatically controlling the elevators.
	 */
	public int GetTarget (int elevatorId) {
		// Service people inside elevator first but stop to let people enter
		if(mData.mElevatorList[elevatorId].mCommittedDirection == IElevator.ELEVATOR_DIRECTION_UP) {
			// Elevator going up, check upwards
			for(int i = mData.mElevatorList[elevatorId].mFloor; i < mData.mBuilding.mNumFloors; i++) {
				if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON || mData.mFloorBtnsUpState[i] == ButtonState.ON) {
					return i;
				}
			}
		} else if(mData.mElevatorList[elevatorId].mCommittedDirection == IElevator.ELEVATOR_DIRECTION_DOWN) {
			// Elevator going down, check downwards
			for(int i = mData.mElevatorList[elevatorId].mFloor; i >= 0; i--) {
				if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON || mData.mFloorBtnsDownState[i] == ButtonState.ON) {
					return i;
				}
			}
		} else if(mData.mElevatorList[elevatorId].mCommittedDirection == IElevator.ELEVATOR_DIRECTION_UNCOMMITTED) {
			// Elevator stopped, check closest stop in both directions
			int closestElevatorDown = Integer.MAX_VALUE;
			int closestElevatorUp = Integer.MAX_VALUE;
			
			for(int i = mData.mElevatorList[elevatorId].mFloor; i < mData.mBuilding.mNumFloors; i++) {
				if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON) {
					closestElevatorUp = i - mData.mElevatorList[elevatorId].mFloor;
				}
			}
			
			for(int i = mData.mElevatorList[elevatorId].mFloor; i >= 0; i--) {
				if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON) {
					closestElevatorDown = mData.mElevatorList[elevatorId].mFloor - i;
				}
			}
			
			if(closestElevatorUp != Integer.MAX_VALUE || closestElevatorDown != Integer.MAX_VALUE) {
				// A button inside elevator is pressed
				if(closestElevatorUp < closestElevatorDown) {
					//Elevator should go up
					for(int i = mData.mElevatorList[elevatorId].mFloor + 1; i < mData.mBuilding.mNumFloors; i++) {
						if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON || mData.mFloorBtnsUpState[i] == ButtonState.ON) {
							return i;
						}
					}
				} else {
					// Elevator should go down
					for(int i = mData.mElevatorList[elevatorId].mFloor - 1 ; i >= 0; i--) {
						if(mData.mElevatorStopState[elevatorId][i] == ButtonState.ON || mData.mFloorBtnsDownState[i] == ButtonState.ON) {
							return i;
						}
					}
				}
			} else {
				// No button inside elevator pressed, check building buttons now
				// Check upwards
				for(int i = mData.mElevatorList[elevatorId].mFloor + 1; i < mData.mBuilding.mNumFloors; i++) {
					if(mData.mFloorBtnsUpState[i] == ButtonState.ON) {
						closestElevatorUp = i - mData.mElevatorList[elevatorId].mFloor;
					}
				}
				
				// Check downwards
				for(int i = mData.mElevatorList[elevatorId].mFloor - 1; i >= 0; i--) {
					if(mData.mFloorBtnsDownState[i] == ButtonState.ON) {
						closestElevatorDown = mData.mElevatorList[elevatorId].mFloor - i;
					}
				}
				
				if(closestElevatorUp != Integer.MAX_VALUE || closestElevatorDown != Integer.MAX_VALUE) {
					// A button inside the building is pressed
					if(closestElevatorUp < closestElevatorDown) {
						return mData.mElevatorList[elevatorId].mFloor + closestElevatorUp;
					} else {
						return mData.mElevatorList[elevatorId].mFloor - closestElevatorDown;
					}
				}
			}
			
		}
		return mData.mElevatorList[elevatorId].mFloor;
	}
}
