/**
* @authors: Kiesenhofer Michael - S1510567007; Rechbauer Susanne - S1510567015; Tunca Mustafa - S1510567021
* @date: 22.01.2017
*/
package sqe.elevator.gruppeD;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import sqe.elevator.gruppeD.data.ButtonState;
import sqe.elevator.gruppeD.data.ElevatorData;
import sqe.elevator.gruppeD.data.ElevatorState;
import sqelevator.IElevator;

/**
 * 
 * @author GruppeD Kiesenhofer, Rechbauer, Tunca
 * This class generates test data for controlling the elevators 
 * by using the IElevator interface
 */
public class DummyInterface implements IElevator {
	private int mNumberOfElevators;
	private int mNumberOfFloors;
	private int mFloorHeight;
	
	private int[] mDirection;
	private int[] mPreviousDoorStatus;
	private float[] mPosition;
	private boolean[][] mServicesFloor;
	private int[] mTarget;
	
	private int mTimeInterval_ns;
	private long mTimeUpdate;
	
	private ElevatorState[] mState;
	private ElevatorData[]  mData;
	private boolean[] 		mTargetLoad;
	
	private ButtonState[][] mStopBtns;
	private ButtonState[] mUpBtns;
	private ButtonState[] mDownBtns;
	
	/**
	 * create randomly the number of elevators, floors
	 */
	public DummyInterface(int interval_ms) {
		mNumberOfElevators = ThreadLocalRandom.current().nextInt(1, 5 + 1);
		mNumberOfFloors = ThreadLocalRandom.current().nextInt(5, 20 + 1);
		mFloorHeight = ThreadLocalRandom.current().nextInt(1, 5 + 1);
		
		mTimeInterval_ns = interval_ms*1000;
		
		mState = new ElevatorState[mNumberOfElevators];
		Arrays.fill(mState, ElevatorState.RUNNING);
		
		mData = new ElevatorData[mNumberOfElevators];
		for (int i=0; i<mNumberOfElevators; ++i) {
			mData[i] = new ElevatorData();
		}
		
		mStopBtns = new ButtonState[mNumberOfElevators][mNumberOfFloors];
		for (int i=0; i<mNumberOfElevators; ++i) {
			for (int j=0; j<mNumberOfFloors; ++j) {
				mStopBtns[i][j] = ButtonState.OFF;
			}
		}
		
		mUpBtns = new ButtonState[mNumberOfFloors];
		Arrays.fill(mUpBtns, ButtonState.OFF);
		
		mDownBtns = new ButtonState[mNumberOfFloors];
		Arrays.fill(mDownBtns, ButtonState.OFF);
		
		mTargetLoad = new boolean[mNumberOfElevators];
		Arrays.fill(mTargetLoad, false);
		
		mDirection = new int[mNumberOfElevators];
		Arrays.fill(mDirection, ELEVATOR_DIRECTION_UNCOMMITTED);
		
		mPreviousDoorStatus = new int[mNumberOfElevators];
		Arrays.fill(mPreviousDoorStatus, ELEVATOR_DOORS_CLOSED);
		
		mPosition = new float[mNumberOfElevators];
		Arrays.fill(mPosition, 0);
		
		mTarget = new int[mNumberOfElevators];
		Arrays.fill(mTarget, 0);

		mServicesFloor = new boolean[mNumberOfElevators][mNumberOfFloors];
		for(int i = 0; i < mNumberOfElevators; i++) {
			for(int k = 0; k < mNumberOfFloors; k++) {
				mServicesFloor[i][k] = ThreadLocalRandom.current().nextBoolean();
			}
		}
		
		mTimeUpdate = System.nanoTime();
	}
	
	/**
	 * update elevator data
	 */
	private void Update () {
		if (System.nanoTime() > mTimeUpdate + mTimeInterval_ns) {
			for (int elev=0; elev<mNumberOfElevators; ++elev) {
				switch (mState[elev]) {
				case RUNNING:
					if (mData[elev].mAcceleration > 0 && mData[elev].mFloor < mNumberOfFloors-1) {
						mData[elev].mFloor += mData[elev].mAcceleration;
						mData[elev].mPosition += mData[elev].mAcceleration * mFloorHeight;
					}
					else if (mData[elev].mAcceleration < 0 && mData[elev].mFloor > 0) {
						mData[elev].mFloor += mData[elev].mAcceleration;
						mData[elev].mPosition += mData[elev].mAcceleration * mFloorHeight;
					}
					
					if (mData[elev].mFloor == mData[elev].mTarget) {
						mState[elev] = ElevatorState.TARGET_ARRIVING;
						if (mData[elev].mSpeed > 0) {
							mUpBtns[mData[elev].mFloor] = ButtonState.OFF;
						}
						else {
							mDownBtns[mData[elev].mFloor] = ButtonState.OFF;
						}
					}
					break;
				case TARGET_ARRIVING:
					mData[elev].mDoorStatus = IElevator.ELEVATOR_DOORS_OPENING;
					mData[elev].mAcceleration = 0;
					mData[elev].mSpeed = 0;
					mState[elev] = ElevatorState.TARGET;
					break;
				case TARGET:
					// open doors, reset stop btn
					mData[elev].mDoorStatus = IElevator.ELEVATOR_DOORS_OPEN;
					mStopBtns[elev][mData[elev].mFloor] = ButtonState.OFF;
					
					if (!mTargetLoad[elev]) {
						int prevWeight = mData[elev].mWeight;
						mData[elev].mWeight = ThreadLocalRandom.current().nextInt(0, 8)*80;
						int cntStop = (mData[elev].mWeight - prevWeight) / 80;
						int x = 0;
						// set new stop btns for people getting on the elevator
						for (int i=0; i<cntStop; ++i) {
							x = ThreadLocalRandom.current().nextInt(0, mNumberOfFloors);
							if (x != mData[elev].mFloor) {
								mStopBtns[elev][x] = ButtonState.ON;
							}
						}
						mTargetLoad[elev] = true;
					}
					
					// if new target is set, close doors -> leave
					if (mData[elev].mFloor != mData[elev].mTarget) {
						mData[elev].mDoorStatus = IElevator.ELEVATOR_DOORS_CLOSING;
						mState[elev] = ElevatorState.TARGET_LEAVING;
						mTargetLoad[elev] = false;
					}
					break;
				case TARGET_LEAVING:
					// close doors, set acc and speed
					mData[elev].mDoorStatus = IElevator.ELEVATOR_DOORS_CLOSED;
					if (mData[elev].mFloor > mData[elev].mTarget) {
						mData[elev].mAcceleration = -1;
						mData[elev].mSpeed = -10;
					}
					else {
						mData[elev].mAcceleration = 1;
						mData[elev].mSpeed = 10;
					}
					
					mState[elev] = ElevatorState.RUNNING;
					break;
				}
			}
			
			boolean elevAvailable = false;
			for (int i=0; i<mNumberOfFloors; ++i) {
				// check for elevator
				elevAvailable = false;
				for (int j=0; j<mNumberOfElevators; ++j) {
					if (mData[j].mFloor == i && mState[j] == ElevatorState.TARGET) {
						elevAvailable = true;
					}
				}
				
				if (!elevAvailable) {
					int max = 150;
					if (i != mNumberOfFloors-1 && ThreadLocalRandom.current().nextInt(1, max) > (max-2)) {
						mUpBtns[i] = ButtonState.ON;
					}
					
					if (i != 0 && ThreadLocalRandom.current().nextInt(1, max) > (max-2)) {
						mDownBtns[i] = ButtonState.ON;
					}
				}
			}
			
			mTimeUpdate = System.nanoTime();
		}		
	}
	
	
	/**
	 * the description of the functions overridden below, are described in IElevator.java
	 */
	@Override
	public int getCommittedDirection(int elevatorNumber) throws RemoteException {
		return mData[elevatorNumber].mCommittedDirection;
	}

	@Override
	public int getElevatorAccel(int elevatorNumber) throws RemoteException {
		return mData[elevatorNumber].mAcceleration;
	}

	@Override
	public boolean getElevatorButton(int elevatorNumber, int floor) throws RemoteException {
		Update();
		
		return (mStopBtns[elevatorNumber][floor] == ButtonState.ON);
	}

	@Override
	public int getElevatorDoorStatus(int elevatorNumber) throws RemoteException {
		Update();
		
		return mData[elevatorNumber].mDoorStatus;
	}

	@Override
	public int getElevatorFloor(int elevatorNumber) throws RemoteException {
		Update();
		
		return mData[elevatorNumber].mFloor;
	}

	@Override
	public int getElevatorNum() throws RemoteException {
		return mNumberOfElevators;
	}

	@Override
	public int getElevatorPosition(int elevatorNumber) throws RemoteException {
		Update();

		return mData[elevatorNumber].mPosition;
	}

	@Override
	public int getElevatorSpeed(int elevatorNumber) throws RemoteException {
		Update();

		return mData[elevatorNumber].mSpeed;
	}

	@Override
	public int getElevatorWeight(int elevatorNumber) throws RemoteException {
		Update();

		return mData[elevatorNumber].mWeight;
	}

	@Override
	public boolean getFloorButtonDown(int floor) throws RemoteException {
		Update();
		
		return (mDownBtns[floor] == ButtonState.ON);
	}

	@Override
	public boolean getFloorButtonUp(int floor) throws RemoteException {
		Update();
		
		return (mUpBtns[floor] == ButtonState.ON);
	}

	@Override
	public int getFloorHeight() throws RemoteException {
		return mFloorHeight;
	}

	@Override
	public int getFloorNum() throws RemoteException {
		return mNumberOfFloors;
	}

	@Override
	public boolean getServicesFloors(int elevatorNumber, int floor) throws RemoteException {
		return mServicesFloor[elevatorNumber][floor];
	}

	@Override
	public int getTarget(int elevatorNumber) throws RemoteException {
		return mData[elevatorNumber].mTarget;
	}

	@Override
	public void setCommittedDirection(int elevatorNumber, int direction) throws RemoteException {
		mData[elevatorNumber].mCommittedDirection = direction;
	}

	@Override
	public void setServicesFloors(int elevatorNumber, int floor, boolean service) throws RemoteException {
		mServicesFloor[elevatorNumber][floor] = service;
	}

	@Override
	public void setTarget(int elevatorNumber, int target) throws Exception {
		if(target >= mNumberOfFloors) {
			throw new Exception("Invalid target");
		}
		if (mState[elevatorNumber] == ElevatorState.TARGET) {
			mData[elevatorNumber].mTarget = target;
		}
	}

	@Override
	public long getClockTick() throws RemoteException {
		return mTimeInterval_ns/1000;
	}

	@Override
	public int getElevatorCapacity(int elevatorNumber) throws RemoteException {
		return 0;
	}

}
